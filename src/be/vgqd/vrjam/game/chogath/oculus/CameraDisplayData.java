package be.vgqd.vrjam.game.chogath.oculus;

import java.awt.Toolkit;

import javafx.geometry.Point2D;

public class CameraDisplayData {
	static {

		SCREENWIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
		SCREENHEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
		ADJUSTMENT =  Toolkit.getDefaultToolkit().getScreenSize().width/10;
	}
	private static  int ADJUSTMENT;
	public static final int SCREENWIDTH;
	public static final int SCREENHEIGHT;
	public static final int MINWIDTH=1280;
	public static final int MINHEIGHT=800;

	public static final Point2D position = new Point2D(SCREENWIDTH / 2,
			SCREENHEIGHT / 2);
	
	public static final Point2D centerLeft = new Point2D(SCREENWIDTH / 4,
			SCREENHEIGHT / 2);
	public static final Point2D centerRight = new Point2D(SCREENWIDTH * 3 / 4,
			SCREENHEIGHT / 2);
	
	public static final void setAdjustment(int value) {
		ADJUSTMENT = value;
	}
	public static final int getAdjustment() {
		return ADJUSTMENT;
	}
	public static final double getWidth(){return SCREENWIDTH/2;}
	public static final double getHeight(){return SCREENHEIGHT;}
	
	
	public static final Point2D getPosition() {
		return position;
	}
	public static final Point2D getCenterleft() {
		return centerLeft;
	}
	public static final Point2D getCenterRight() {
		return centerRight;
	}
	
	
	
	
	
}
