package be.vgqd.vrjam.game.chogath.oculus.x360;

import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import be.vgqd.vrjam.game.chogath.utils.Direction;

public class X360ControllerData {

	private static Controller x360;
	private static X360ControllerData INSTANCE = null;

	public static X360ControllerData getInstance() {
		if (INSTANCE == null)
			INSTANCE = new X360ControllerData();
		return INSTANCE;
	}

	static {
		for (Controller c : ControllerEnvironment.getDefaultEnvironment()
				.getControllers())
			if (c.getType() == Controller.Type.GAMEPAD) {
				x360 = c;
			}

	}

	public static Controller getX360Instance() {
		return getX360Instance(false);
	}

	public static Controller getX360Instance(boolean refresh) {

		if (refresh == true) {
			x360 = null;
			for (Controller c : ControllerEnvironment.getDefaultEnvironment()
					.getControllers())
				if (c.getType() == Controller.Type.GAMEPAD) {
					x360 = c;

				}
		}

		return x360;

	}

	protected X360ControllerData() {

	}

	/** A = 0: 0<->1 */
	private boolean a;

	/** B = 1: 0<->1 */
	private boolean b;

	/** X = 2: 0<->1 */
	private boolean x;

	/** Y = 3: 0<->1 */
	private boolean y;

	/** LB = 4: 0<->1 */
	private boolean lb;

	/** RB = 5: 0<->1 */
	private boolean rb;

	/** MENU = 6: 0<->1 */
	private boolean menu;

	/** START = 7: 0<->1 */
	private boolean start;

	/** LEFT TRIGGER LT =0 (no pression->1 full pression) */
	private double lt;

	/** Right TRIGGER RT =0 (no pression->1 full pression) */
	private double rt;

	/** Press stick Left= 8: 0<->1 */
	private boolean leftPull;

	/** Press stick Right = 9: 0<->1 */
	private boolean rightPull;

	private final Direction arrows = new Direction();
	private final Direction left = new Direction();
	private final Direction right = new Direction();

	public boolean isA() {
		return a;
	}

	public void setA(boolean a) {
		this.a = a;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}

	public boolean isX() {
		return x;
	}

	public void setX(boolean x) {
		this.x = x;
	}

	public boolean isY() {
		return y;
	}

	public void setY(boolean y) {
		this.y = y;
	}

	public boolean isLb() {
		return lb;
	}

	public void setLb(boolean lb) {
		this.lb = lb;
	}

	public boolean isRb() {
		return rb;
	}

	public void setRb(boolean rb) {
		this.rb = rb;
	}

	public boolean isMenu() {
		return menu;
	}

	public void setMenu(boolean menu) {
		this.menu = menu;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

	public double getLt() {
		return lt;
	}

	public void setLt(double lt) {
		if (lt < 0)
			lt = 0.0;
		if (lt > 1)
			lt = 1.0;
		this.lt = lt;
	}

	public double getRt() {
		return rt;
	}

	public void setRt(double rt) {
		if (rt < 0)
			rt = 0.0;
		if (rt > 1)
			rt = 1.0;
		this.rt = rt;
	}

	public Direction getArrows() {
		return arrows;
	}

	public void setArrows(double value) {
		double x = 0, y = 0, intensity = 0, tmp = 0;

		if (value > 0) {
			intensity = 1.0;
			x = (value > 0.5 ? 0.5 - value % 0.5 : value) * 4 - 1;
			tmp = Math.abs(value - 0.25);
			y = -((tmp > 0.5 ? 0.5 - tmp % 0.5 : tmp) * 4 - 1);
		} else {
			x = 0.0;
			y = 0.0;
		}
		this.arrows.setIntensity(intensity);
		this.arrows.setX(x);
		this.arrows.setY(y);

	}

	public boolean isLeftPull() {
		return leftPull;
	}

	public void setLeftPull(boolean leftPull) {
		this.leftPull = leftPull;
	}

	public boolean isRightPull() {
		return rightPull;
	}

	public void setRightPull(boolean rightPull) {
		this.rightPull = rightPull;
	}

	public Direction getLeft() {
		return left;
	}

	public void setLeft(Direction left) {
		this.left.setX(left.getX());
		this.left.setY(left.getY());
	}

	public Direction getRight() {
		return right;
	}

	public void setRight(Direction right) {
		this.right.setX(right.getX());
		this.right.setY(right.getY());

	}

	public static boolean isConnected() {

		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("X360ControllerData [");

		if (a) {
			builder.append("\t A");
		}
		if (b) {
			builder.append("\t B");
		}
		if (x) {
			builder.append("\t X");
		}
		if (y) {
			builder.append("\t Y");
		}
		if (lb) {
			builder.append("\t LB");
		}
		if (rb) {
			builder.append("\t RB");
		}
		if (menu) {
			builder.append("\t menu");
		}
		if (start) {
			builder.append("\t start");
		}
		if (lt > 0.1) {
			builder.append("\t LT=");
			builder.append(lt);
		}
		if (rt > 0.1) {
			builder.append("\t RT=");
			builder.append(rt);
		}
		if (leftPull) {
			builder.append("\t left ->O");
		}
		if (rightPull) {
			builder.append("\t right ->O");
		}
		if (Math.abs(arrows.getX()) > 0.1 || Math.abs(arrows.getY()) > 0.1) {
			builder.append("\t <-�->=");
			builder.append(arrows);
		}
		if (Math.abs(left.getX()) > 0.1 || Math.abs(left.getY()) > 0.1) {
			builder.append("\t left=");
			builder.append(left);
		}
		if (Math.abs(right.getX()) > 0.1 || Math.abs(right.getY()) > 0.1) {
			builder.append("\t right=");
			builder.append(right);
		}
		builder.append("]");
		return builder.toString();
	}


}
