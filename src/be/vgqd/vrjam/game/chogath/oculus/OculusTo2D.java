package be.vgqd.vrjam.game.chogath.oculus;

import be.vgqd.vrjam.game.chogath.utils.Direction;

public class OculusTo2D {
	private final static Direction lastDirection = new Direction();

	/**
	 * Return a singleton witch describe the last direction calculated of the
	 * Oculus Rift( can be useful to reduce instance creation)
	 */
	public Direction getLastDirectionInstance() {
		return lastDirection;
	}

	/**
	 * This methode give the direction where the player is looking base on the
	 * initial position RYP(0.0)
	 */
	public final static Direction getDirection(double roll, double yaw,
			double pitch) {
		float x = 0, y = 0;
		Direction dir = new Direction();
		x = (float) (-1f * Math.cos(yaw - Math.PI / 2));
		y = (float) Math.sin(pitch);
		dir.setX(x);
		dir.setY(y);
		dir.setIntensity(getIntensity(roll, yaw, pitch));

		return dir;
	}

	/**
	 * More the the Oculus looks in the corner of the 2D transformation, more
	 * the intensity is.
	 */
	public final static float getIntensity(double roll, double yaw, double pitch) {
		double intHorizontal = 0., intVertical = 0.;

		// Intensity of the yaw: Horizontal
		if (Math.abs(yaw) > Math.PI / 2)
			yaw = yaw < 0 ? -1 : 1 * Math.PI / 2;
		intHorizontal = yaw / (Math.PI / 2);

		// Intensity of the pitch: Vertical
		if (Math.abs(pitch) > Math.PI / 2)
			pitch = pitch < 0 ? -1 : 1 * Math.PI / 2;
		intVertical = pitch / (Math.PI / 2);

		return (float) ((intHorizontal + intVertical) / 2);
	}

	public static Direction getDirection(RYP frontview, double roll, double yaw,
			double pitch) {

		return getDirection(roll+-frontview.getRoll(), yaw+-frontview.getYaw(), pitch+-frontview.getPitch());
	}

	public static Direction getDirection(RYP frontview, RYP lastPositionRYP) {
		return getDirection(frontview, lastPositionRYP.getRoll(), lastPositionRYP.getYaw(), lastPositionRYP.getPitch());
		
	}

}
