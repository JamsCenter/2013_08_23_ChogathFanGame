package be.vgqd.vrjam.game.chogath.oculus;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;

public class OculusScene extends Scene {
	public enum Camera {
		Left, Right
	};

	private final Group root;
	private final Group camLeft, camRight;

	public OculusScene(Group parent) {
		super(parent);
		final Color backGroundColor = Color.BLACK;
		final int blinderSize = CameraDisplayData.SCREENHEIGHT * 4 / 9;
		@SuppressWarnings("rawtypes")
		CircleBuilder cb = CircleBuilder.create().radius(blinderSize)
				.layoutX(CameraDisplayData.SCREENWIDTH / 4)
				.layoutY(CameraDisplayData.SCREENHEIGHT / 2);
		final Circle blinderLeft = cb.build();
		final Circle blinderRight = cb.build();

		this.setFill(backGroundColor);

		root = new Group();
		camLeft = new Group();
		camLeft.setClip(blinderLeft);

		camRight = new Group();
		camRight.setLayoutX(CameraDisplayData.SCREENWIDTH / 2);
		camRight.setClip(blinderRight);

		blinderLeft.setEffect(new GaussianBlur(100));
		blinderRight.setEffect(new GaussianBlur(100));

		parent.getChildren().add(root);
		root.getChildren().addAll(camLeft, camRight);

	}

	public StereoNode<Node> add(Node left, Node right) {
		if (left == null)
			left = new Pane();
		if (right == null)
			right = new Pane();
		return add(left, right, (left.getLayoutX() + right.getLayoutX()) / 2,
				(left.getLayoutY() + right.getLayoutY()) / 2);
	}

	public StereoNode<Node> add(Node left, Node right, double x, double y) {
		return add(left, right, x, y, 0);
	}

	public StereoNode<Node> add(Node left, Node right, double x, double y,
			double deepness) {
		StereoNode<Node> sn = StereoNodeBuilder.getOculusStereoNode(left,
				right, x, y, deepness);

		camLeft.getChildren().add(left);
		camRight.getChildren().add(right);

		return sn;
	}

	public void cleanCameras() {
		camLeft.getChildren().clear();
		camRight.getChildren().clear();

	}

	public void remove(StereoNode<? extends Node> sn) {
		camLeft.getChildren().remove(sn.getLeft());
		camRight.getChildren().remove(sn.getRight());

	}

	public void add(StereoNode<? extends Node> sn) {
		if (sn != null) {
			camLeft.getChildren().add(sn.getLeft());
			camRight.getChildren().add(sn.getRight());
		}
	}

}
