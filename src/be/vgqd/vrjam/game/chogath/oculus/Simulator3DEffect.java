package be.vgqd.vrjam.game.chogath.oculus;

import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.effect.DropShadowBuilder;

public class Simulator3DEffect {
	
	static private DropShadowBuilder<?> shadow ;
	
	public static final void shadowEffect(StereoNode<Node> sn, int value)
	{
		if(shadow==null)shadow = DropShadowBuilder.create();
		shadow.offsetX(value);
		sn.getRight().setEffect(shadow.build());
		shadow.offsetX(-value);
		sn.getLeft().setEffect(shadow.build());
		
		
	}
	
	public static final void rotationEffect(StereoNode<Node> sn, double value)
	{
		sn.getRight().setRotationAxis(new Point3D(value, value, value));
		sn.getLeft().setRotationAxis(new Point3D(value, value, value));
		
		
	}

}
