package be.vgqd.vrjam.game.chogath.game.images;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.logging.Logger;

import be.vgqd.vrjam.game.chogath.data.Paths;

import javafx.scene.image.Image;

public class Images {

	private static Logger log = Logger.getLogger(Images.class.getName());
	private static HashMap<String, Image> imagesUsed = new HashMap<>();

	public static Image getImage(String path) {
		Image img = null;
		if ((img = imagesUsed.get(path)) != null)
			return img;

		try {

			File f = new File(Paths.IMAGE_PATH+path);
			if (f.exists() && f.isFile()) {

				img = new Image(new FileInputStream(f));
				imagesUsed.put(path, img);
				
			}
		} catch (Exception e) {
			log.warning("Exception the file is not found, impossible to build the image");
		}
		return img;

	}

}
