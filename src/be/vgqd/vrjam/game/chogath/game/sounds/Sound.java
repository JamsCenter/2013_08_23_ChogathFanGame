package be.vgqd.vrjam.game.chogath.game.sounds;

import java.util.logging.Logger;

import javafx.scene.media.AudioClip;

public class Sound {
	
	private static final Logger log = Logger.getLogger(Sound.class.getName());
	public enum SoundType {
		AMBIANCE, SOUND, INTERFACE
	}

	private static AudioClip ambianceMusic;

	public static void play(String soundFileName, SoundType type) {

		play(soundFileName, type, 1.0);
	}
	public static void play(String soundFileName, SoundType type, double d) {
		try {
			if (soundFileName != null)
				play(new AudioClip(Sound.class.getResource(soundFileName)
						.toString()), type,d);
			}catch (Exception e)
			{
				e.printStackTrace();
				log.warning("Excetpion:"+e.getMessage());
			}
		
	}

	public static void play(AudioClip sound, SoundType type, double volume) {

		if (type == SoundType.AMBIANCE) {
			if (ambianceMusic != null) {
				ambianceMusic.stop();
				ambianceMusic = null;
			}
			ambianceMusic = sound;
			ambianceMusic.setCycleCount(100);
			sound=ambianceMusic;
		}
		sound.setVolume(volume);
		sound.play();
	

	}
	public static void stop() {
	
		ambianceMusic.stop();
	}



}
