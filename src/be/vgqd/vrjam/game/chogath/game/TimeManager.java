package be.vgqd.vrjam.game.chogath.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javafx.animation.AnimationTimer;

public class TimeManager {

	private static HashMap<Object, Timer> timers = new HashMap<>();

	public static void store(Object o, Timer time) {
		if (o == null || time == null)
			throw new NullPointerException();
		timers.put(o, time);

	}

	public static Timer get(Object o) {
		return timers.get(o);
	}

	public interface TimerListener {
		public abstract void ticTac();

	}

	public static class Timer extends AnimationTimer {
		public Timer(int fps) {
			if (fps < 20)
				fps = 20;
			this.ms = 1 / fps;
		}

		private int ms;
		private List<TimerListener> listeners = new ArrayList<>();

		@Override
		public void handle(long arg0) {

			for (TimerListener l : listeners) {
				l.ticTac();
			}

			try {
				Thread.sleep(ms);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		public void addListener(TimerListener listener) {
			if (listener != null) {
				listeners.add(listener);
			}
		}

		public double getMS() {
			return ms;
		}

	
		public void removeListener() {
			listeners.clear();
			
		}

	}

	private TimeManager() {
		throw new IllegalStateException("");
	}

	public static Collection<Timer> getTimers() {
		return timers.values();

	}

	public static void clean() {
//		for (Timer t : TimeManager.getTimers()) {
//			t.stop();
//		}
//		timers.clear();

	}

}
