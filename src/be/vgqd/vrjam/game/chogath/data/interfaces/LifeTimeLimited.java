package be.vgqd.vrjam.game.chogath.data.interfaces;

public interface LifeTimeLimited {
	public int getLifeTimeInMS();

}
