package be.vgqd.vrjam.game.chogath.data;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Random;

import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.InGameData;
import be.vgqd.vrjam.game.chogath.game.TimeManager.TimerListener;

public class GamePopAndTimeManager implements TimerListener {

	private static GamePopAndTimeManager INSTANCE = new GamePopAndTimeManager();

	public static GamePopAndTimeManager getInstance() {
		return INSTANCE;
	}

	private boolean recordData;
	private long lastRefreshtime;
	private long playedTime;
	private long startGame;

	private Boss[] boss;
	private long[] bossLastTime;

	private long championLastTime = 0;
	private int level = 0;
	private Random rand = new Random();

	@Override
	public void ticTac() {
		if (recordData && !InGame.getInstance().isInPause()) {
			InGame.getInstance().refreshTimeNode(playedTime);
			if ((int) (playedTime / GameData.TIME_DIFFICULTY_LEVELUP) > level) {
				InGame.getInstance().addOneLevelOfDifficulty();

			}

			long now = GregorianCalendar.getInstance().getTimeInMillis();
			playedTime += now - lastRefreshtime;
			lastRefreshtime = now;

			double difficulty = InGameData.getInstance().getDifficulty();

			double timeToPop = 0.0;
			Boss boss = null;
			long lastTimeAttack = 0;
			double nombre = 0.0;

			level = InGameData.getInstance().getLevel();

			for (int i = 0; i < this.boss.length; i++) {
				boss = this.boss[i];
				lastTimeAttack = this.bossLastTime[i];
				if (level >= boss.getPrefLevelToPop()
						&& boss.getBullet() != null) {
					if (!boss.isPopped() && boss.isAlive()) {
						InGame.getInstance().addBoss(boss);
					}

					if (boss.isPopped() && (!boss.isAlive() || boss.isHide())) {

						timeToPop = boss.getBulletRandomPopFrequenceTime();
						timeToPop *= 1.0 - difficulty / 2.0;
						if (now - lastTimeAttack > timeToPop) {
							this.bossLastTime[i] = now;
							nombre = (double) level / 25.0;
							if (boss.isHide() && rand.nextInt(3) == 2)
								nombre *= 2.0;

							for (int j = 0; j < nombre; j++) {
								InGame.getInstance()
										.addBullet(boss.getBullet());

							}

						}
					}

				}
			}

			timeToPop = GameData.POP_CHAMPION_TIME;
			if (now - championLastTime > timeToPop) {
				championLastTime = now;
				InGame.getInstance().addChampion();

			}

		}

	}

	public boolean isRecordingData() {
		return recordData;
	}

	public void startRecoardingData() {
		this.recordData = true;
		startGame = GregorianCalendar.getInstance().getTimeInMillis();
		lastRefreshtime = startGame;

	}

	public void stopRecordingData() {
		this.recordData = false;
	}

	public static void clean() {
		getInstance().recordData = false;
		getInstance().lastRefreshtime = 0;
		getInstance().playedTime = 0;
		getInstance().startGame = 0;

		getInstance().level = 0;

		cleanInstance();
	}

	private static void cleanInstance() {
		Collection<Boss> bosses = Boss.values();
		INSTANCE.boss = new Boss[bosses.size()];
		INSTANCE.bossLastTime = new long[bosses.size()];
		int i = 0;
		for (Boss b : bosses) {
			INSTANCE.boss[i++] = b;
		}

	}

	public long getTimePlayed() {
		return playedTime;
	}

}
