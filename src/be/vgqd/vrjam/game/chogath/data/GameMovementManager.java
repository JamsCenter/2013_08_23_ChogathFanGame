package be.vgqd.vrjam.game.chogath.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javafx.scene.Node;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet;
import be.vgqd.vrjam.game.chogath.data.modele.InGameData;
import be.vgqd.vrjam.game.chogath.data.modele.Map;
import be.vgqd.vrjam.game.chogath.data.modele.Player;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.game.TimeManager.TimerListener;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.utils.Direction;

public class GameMovementManager implements TimerListener {

	// private static Logger log = Logger.getLogger(GameMovementManager.class
	// .getName());

	static final private GameMovementManager INSTANCE = new GameMovementManager();

	public static GameMovementManager getInstance() {
		return INSTANCE;
	}

	private GameMovementManager() {
		movingObject.clear();
	}

	private final HashMap<Object, MovingOject> movingObject = new HashMap<>();
	private boolean isOkToMove;

	public static void store(Object o, Direction d, double speed) {
		INSTANCE.addMovingOject(o, d, speed);
	}

	public void addMovingOject(Object o, Direction d, double speed) {
		if (o != null && d != null) {
			movingObject.put(o, new MovingOject(d, speed));
		}
	}

	public static void removeMovingOjectOfInstance(Object o) {
		INSTANCE.removeMovingOject(o);
	}

	public void removeMovingOject(Object o) {
		if (o != null) {
			movingObject.remove(o);
		}
	}

	private final double time = 1.0 / GameData.FPS_REFRESH_IMPORTAN_OBJECT;

	public void move(Object o) {

		MovingOject mo = null;
		if (o != null && (mo = movingObject.get(o)) != null) {

			StereoNode<Node> sn = GUI.get(o);
			if (sn != null) {
				Direction d = mo.getDirection();

				if ((Math.abs(d.getX()) > 0.05 || Math.abs(d.getY()) > 0.05)
						&& d.getIntensity() > 0.05) {
					double movex = d.getX() * mo.getSpeed() * time;
					double movey = d.getY() * mo.getSpeed() * time;
					double x = sn.getLayoutX() + movex;
					double y = sn.getLayoutY() - movey;

					if (!InGameData.getInstance().getMap().isMapBoundsOk(x, y)) {
						if (o instanceof Player) {
							Map m = InGameData.getInstance().getMap();

							if (x < 0)
								x = 0;
							if (y < 0)
								y = 0;
							if (x > m.getWidth())
								x = m.getWidth();
							if (y > m.getHeight())
								y = m.getHeight();

						} else if (o instanceof Boss) {
							Map m = InGameData.getInstance().getMap();
							if (x < 0)
								d.inverseX();
							if (y < 0)
								d.inverseY();
							if (x > m.getWidth())
								d.inverseX();
							if (y > m.getHeight())
								d.inverseY();

						} else if (o instanceof Bullet
								&& ((Bullet) o).getType() == Bullet.Type.LASER) {
							Map m = InGameData.getInstance().getMap();
							if (x < 0)
								d.inverseX();
							if (y < 0)
								d.inverseY();
							if (x > m.getWidth())
								d.inverseX();
							if (y > m.getHeight())
								d.inverseY();

						} else {
							InGame.getInstance().askToRemove(o);
						}
					}
					sn.setLayout(x, y);
				}
			}
		}
	}

	@Override
	public synchronized void ticTac() {

		if (isOkToMove) {
		
				Object o = null;

				Iterator<Object> objIt = movingObject.keySet().iterator();

				o = null;
				while (objIt.hasNext()) {
					o = objIt.next();
					INSTANCE.move(o);

				}

				Iterator<Object> remIt = removeList.iterator();

				o = null;
				while (remIt.hasNext()) {
					o = remIt.next();
					movingObject.remove(o);
					remIt.remove();
				}
			
		}
	}

	private static Vector<Object> removeList = new Vector<>();

	public static void remove(Object o) {

		removeList.add(o);
	}

	public static void clean() {
		getInstance().movingObject.clear();

	}

	public void startMovingObject() {
		isOkToMove = true;

	}

	public void stopMovingObject() {
		isOkToMove = false;

	}

	public static class MovingOject {
		private Direction d;
		private double speed;

		public MovingOject(Direction d, double speed) {
			this.d = d;
			this.speed = speed;

		}

		public Direction getDirection() {
			return d;
		}

		public void setD(Direction d) {
			this.d = d;
		}

		public double getSpeed() {
			return speed;
		}

		public void setSpeed(double speed) {
			this.speed = speed;
		}

	}

	public Direction getDirectionOf(Object o) {
		MovingOject mo = movingObject.get(o);
		if (mo != null)
			return mo.d;
		return null;

	}

	public void setSpeedTo(Object o, int speed) {
		MovingOject mo = movingObject.get(o);
		if (mo != null)
			mo.setSpeed(speed);
	}
}
