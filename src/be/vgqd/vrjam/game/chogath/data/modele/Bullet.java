package be.vgqd.vrjam.game.chogath.data.modele;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import be.vgqd.vrjam.game.chogath.data.GameBossMoveManager;
import be.vgqd.vrjam.game.chogath.data.GameData;
import be.vgqd.vrjam.game.chogath.data.GameMovementManager;
import be.vgqd.vrjam.game.chogath.data.GameRefreshManager;
import be.vgqd.vrjam.game.chogath.data.GameSoundManager;
import be.vgqd.vrjam.game.chogath.data.GameSoundManager.GameSound;
import be.vgqd.vrjam.game.chogath.data.InGame;
import be.vgqd.vrjam.game.chogath.data.Paths;
import be.vgqd.vrjam.game.chogath.data.interfaces.LifeTimeLimited;
import be.vgqd.vrjam.game.chogath.data.interfaces.Refreshable;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.nodes.PowerNode;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.utils.Direction;

public abstract class Bullet implements LifeTimeLimited, Refreshable {
	public enum Type {
		JAVELIN, MUSHROOM, NRJ, FIREBALL, CHOSPIKE, CREEPERBALL, PACMANBALL, REQUIEM, DEMANCIA, ROCKET, LASER, BIGFIREBALL, NRJJAVELIN, ATOMICROCKET, EXPLOSION, BUSH, RUPTURE, FERIALSCREAM
	}

	protected Bullet(Type type) {
		this.type = type;
		init();
	};

	protected abstract void init();

	protected Type type = null;
	// -2 = inifinity
	protected int lifeTimeMS = -2;
	protected int dommage = 1;
	protected double speed = GameData.DEFAUTSPEED;
	protected Direction direction = new Direction(0.0, 0.0);
	protected double apparitionMinDistance = 50;
	protected double collisionRadius = 10;
	protected double width = 10, height = 10;
	protected boolean hitPlayer = true;
	protected boolean hitBoss = false;
	protected boolean hitChampion = false;
	protected boolean hitObject = false;
	protected boolean movingBullet = true;

	protected int count = 1;

	protected Direction directionWanted = null;

	/**
	 * If return null, the game found a direction by itself, else the game must
	 * repect its decision
	 */
	public Direction getDirectionWanted() {
		return directionWanted;
	}

	protected Point2D positionWanted = null;

	/**
	 * If return null, the game found a position by itself, else the game must
	 * repect its decision
	 */
	public Point2D getStartPositionWanted() {
		return positionWanted;
	}

	@Override
	public int getLifeTimeInMS() {
		return lifeTimeMS;
	}

	public void refresh(long time) {
		refreshBehabiour(time);
	}

	public void apparitionBehaviour() {
	}

	public void collisionBehaviour(Object o) {
		if (o instanceof Player) {
			Player p = (Player) o;
			p.hit(this.dommage);
			GameSoundManager.play(GameSound.CHOGATH_HIT);
			InGame.getInstance().checkPlayerState();
			InGame.getInstance().askToRemove(this);
		}
	}

	public abstract void disparitionBehaviour();

	public void refreshBehabiour(long time) {
	}

	private static Random r = new Random();

	public static Bullet getBullet(Type type) {
		Bullet b = null;
		if (type == null)
			type = Type.values()[r.nextInt(Type.values().length)];
		if (type == Type.JAVELIN) {

			b = new Bullet(type) {

				@Override
				public void collisionBehaviour(Object o) {
					if (hitPlayer && o instanceof Player) {

						GameSoundManager.play(GameSound.NIDALEE_JAVELIN_HIT);

						super.collisionBehaviour(o);
					}

				}

				@Override
				public void refreshBehabiour(long time) {

				}

				@Override
				public void apparitionBehaviour() {

				}

				@Override
				public void disparitionBehaviour() {
				}

				@Override
				protected void init() {
					this.width = 25;
					this.height = 6;
					this.collisionRadius = 3;
					this.apparitionMinDistance = 100;

				}
			};

		}

		else if (type == Type.MUSHROOM) {

			b = new Bullet(type) {

				@Override
				public void collisionBehaviour(Object o) {
					if (hitPlayer && o instanceof Player) {

						GameSoundManager.play(GameSound.MUSHROOM_DESTROYED);

						super.collisionBehaviour(o);
					}

				}

				private long timePopped;

				@Override
				public void refreshBehabiour(long time) {

					if (timePopped <= 0)
						timePopped = time;
					if (time - timePopped > 1800 + InGameData
							.getRandomInstance().nextInt(3000)) {
						GameMovementManager.remove(this);
						GameRefreshManager.remove(this);
					}

					super.refreshBehabiour(time);
				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);

				}

				@Override
				public void disparitionBehaviour() {
				}

				@Override
				protected void init() {
					this.width = 11;
					this.height = 11;
					this.collisionRadius = 5;
					this.apparitionMinDistance = 30;
					positionWanted = InGame.getInstance().getValideRandomPoint(
							this.apparitionMinDistance);

				}
			};
		} else if (type == Type.CHOSPIKE) {

			b = new Bullet(type) {

			
				@Override
				public void apparitionBehaviour() {

				}

				@Override
				public void disparitionBehaviour() {
				}

				@Override
				protected void init() {
					this.width = 12;
					this.height = 6;
					this.collisionRadius = 3;
					this.speed = 150;
					this.apparitionMinDistance = 150;

				}
			};
		} else if (type == Type.NRJ) {

			b = new Bullet(type) {

				@Override
				public void collisionBehaviour(Object o) {
					if (o instanceof Player) {
						super.collisionBehaviour(o);
					}

				}

				private long lastTime = 0;
				private Direction rotation;
				private Direction initial;

				@Override
				public void refreshBehabiour(long time) {
					lastTime = (time - lastTime) / 4;
					double angle = (lastTime % 360);
					Direction.setDirectionOnAngle(rotation, angle);
					StereoNode<Node> sn = GUI.get(this);
					if (sn != null)
						sn.setRotation(angle);

					direction.joinData(initial, initial, rotation);

				}

				@Override
				public void apparitionBehaviour() {
					initial = new Direction();
					rotation = new Direction();
					direction.setIntensity(1.0);
					rotation.setIntensity(1.0);
					initial.setWith(direction);
					GameRefreshManager.getInstance().store(this, this);

				}

				@Override
				public void disparitionBehaviour() {
					GameRefreshManager.remove(this);

				}

				@Override
				protected void init() {

					this.width = 13;
					this.height = 13;
					this.collisionRadius = 6;
					this.speed = 70;
					this.apparitionMinDistance = 40;

				}
			};
		} else if (type == Type.LASER) {

			b = new Bullet(type) {

				@Override
				protected void init() {

					this.collisionRadius = 3;
					this.height = 10;
					this.width = 10;
					this.speed = 100;
					this.dommage = 1;
					this.apparitionMinDistance = 50;

				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;

					if (time - apparition > 7000) {

						InGame.getInstance().askToRemove(this);

					}

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);
				}
			};

		} else if (type == Type.BIGFIREBALL) {

			b = new Bullet(type) {

				@Override
				protected void init() {
					positionWanted = InGameData.getInstance().getMap()
							.getRanomdCornerPosition();
					this.collisionRadius = 6;
					this.height = 20;
					this.width = 20;
					this.speed = 60;
					this.dommage = 2;

				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;

					StereoNode<Node> sn = GUI.get(this);
					if (sn != null && direction != null) {
						direction.setWith(InGame.getInstance()
								.getPlayerDirection(sn.getLayoutX(),
										sn.getLayoutY()));
						sn.setRotation(360 - direction.getAngle());

						if (time - apparition > 15000) {

							InGame.getInstance().askToRemove(this);

						}
					}

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);
				}
			};

		} else if (type == Type.FIREBALL) {

			b = new Bullet(type) {

				@Override
				protected void init() {

					positionWanted = InGameData.getInstance().getMap()
							.getRanomdCornerPosition();
					this.collisionRadius = 4;
					this.height = 10;
					this.width = 10;
					this.speed = 120;
					this.dommage = 1;
					this.count = 4;

				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;
				private boolean flagUsed;
				private int divisionTime = 600;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;

					if (!flagUsed && time - apparition > divisionTime) {
						flagUsed = true;
						StereoNode<Node> sn = GUI.get(this);

						Bullet b = null;
						int i = --this.count;
						if (i > 0) {
							if (sn != null) {
								b = InGame.getInstance().addBullet(
										Bullet.getBullet(Type.FIREBALL),
										sn.getLayoutX(), sn.getLayoutY());
								if (b != null) {
									b.count = i;
									Direction.setDirectionOnAngle(b.direction,
											direction.getAngle() + 45.0);
									GUI.get(b).setRotation(
											360 - b.direction.getAngle());
								}
								b = InGame.getInstance().addBullet(
										Bullet.getBullet(Type.FIREBALL),
										sn.getLayoutX(), sn.getLayoutY());
								if (b != null) {
									b.count = i;
									Direction.setDirectionOnAngle(b.direction,
											direction.getAngle() - 45.0);
									GUI.get(b).setRotation(
											360 - b.direction.getAngle());

								}
							}
						}

					}

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		} else if (type == Type.ROCKET) {

			b = new Bullet(type) {

				@Override
				protected void init() {

					positionWanted = InGameData.getInstance().getMap()
							.getRandomCenterEdgePosition();
					this.collisionRadius = 4;
					this.height = 10;
					this.width = 10;
					this.speed = 120;
					this.dommage = 2;
					this.count = 1;

				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;
				private boolean flagUsed;
				private int explosionTime = 2000;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;

					if (!flagUsed && time - apparition > explosionTime) {
						flagUsed = true;

						detonation();
						InGame.getInstance().askToRemove(this);

					}

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}

				private void detonation() {
					StereoNode<Node> sn = GUI.get(this);
					if (sn != null) {
						InGame.getInstance().addBullet(
								Bullet.getBullet(Type.EXPLOSION),
								sn.getLayoutX(), sn.getLayoutY());
					}
				}
			};

		} else if (type == Type.EXPLOSION) {

			b = new Bullet(type) {

				@Override
				protected void init() {

					this.collisionRadius = 10;
					this.height = 10;
					this.width = 10;
					this.speed = 120;
					this.dommage = 2;
					this.count = 1;

				}

				@Override
				public void apparitionBehaviour() {
					GameRefreshManager.getInstance().store(this, this);
					GameMovementManager.remove(this);

				}

				private long apparition;
				private double explosionTime = 1000 + InGameData
						.getRandomInstance().nextInt(1000);
				private double random = 1 + InGameData.getRandomInstance()
						.nextInt(2);
				private boolean flaguse = true;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;
					double radius = (double) (time - apparition)
							/ explosionTime * 4 * random;
					StereoNode<Node> sn = GUI.get(this);
					if (sn != null) {
						sn.setScaleX(radius);
						sn.setScaleY(radius);
						sn.setOpacity(radius);
						collisionRadius *= radius * 2 * random;
						if (!flaguse
								&& InGame.getInstance().isCollidingWithPlayer(
										sn.getLayoutX(), sn.getLayoutY(),
										collisionRadius)) {
							collisionBehaviour(InGameData.getInstance()
									.getPlayer());

						}
					}
					if (time - apparition > explosionTime) {

						InGame.getInstance().askToRemove(this);

					}

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		} else if (type == Type.BUSH) {

			b = new Bullet(type) {

				@Override
				protected void init() {
					this.collisionRadius = 60;
					this.height = 80;
					this.width = 100;
					directionWanted = new Direction(0.0, 0.0);
					Map m = InGameData.getInstance().getMap();
					positionWanted = new Point2D(m.getCenterX(), m.getCenterY());

					this.speed = 0;
					this.dommage = 0;
					this.count = 1;

				}

				@Override
				public void collisionBehaviour(Object o) {
					GameBossMoveManager.getInstance().stop(
							Boss.getBoss("Garen"), false);
					InGame.getInstance().askToRemove(this);
				}

				@Override
				public void apparitionBehaviour() {
					GameMovementManager.remove(this);

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		} else if (type == Type.RUPTURE) {

			b = new Bullet(type) {

				@Override
				protected void init() {
					this.collisionRadius = 40;
					this.height = 80;
					this.width = 80;
					directionWanted = new Direction(0.0, 0.0);

					this.speed = 0;
					this.dommage = 1;
					this.hitBoss = true;
					this.hitObject = true;
					this.hitChampion = true;
					this.hitPlayer = false;
					this.count = 1;

				}

				@Override
				public void collisionBehaviour(Object o) {

				}

				@Override
				public void apparitionBehaviour() {
					GameMovementManager.remove(this);
					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;
				private double dommageTime = 500;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;
					double radius = 0.5 + (double) (time - apparition)
							/ dommageTime;
					StereoNode<Node> sn = GUI.get(this);
					if (sn != null)
						sn.setOpacity(radius);

					if (time - apparition > dommageTime) {

						InGame.getInstance().bulletCollision(this);

						InGame.getInstance().askToRemove(this);

					}
				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		} else if (type == Type.FERIALSCREAM) {

			b = new Bullet(type) {

				@Override
				protected void init() {
					this.collisionRadius = 40;
					this.height = 120;
					this.width = 120;

					this.speed = InGameData.getInstance().getPlayer()
							.getSpeed();
					this.dommage = 1;
					this.hitBoss = true;
					this.hitObject = true;
					this.hitChampion = true;
					this.hitPlayer = false;
					this.count = 1;

				}

				@Override
				public void collisionBehaviour(Object o) {

				}

				@Override
				public void apparitionBehaviour() {

					InGame.getInstance().bulletCollision(this);

					GameRefreshManager.getInstance().store(this, this);

				}

				private long apparition;
				private double dommageTime = 1000;

				@Override
				public void refreshBehabiour(long time) {
					if (apparition == 0)
						apparition = time;

					InGame.getInstance().bulletCollision(this);

					double radius = 0.5 + (double) (time - apparition)
							/ dommageTime;
					StereoNode<Node> sn = GUI.get(this);
					if (sn != null)

						sn.setOpacity(radius);

					if (time - apparition > dommageTime) {

						InGame.getInstance().askToRemove(this);

					}
				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		} else if (type == Type.DEMANCIA) {

			b = new Bullet(type) {

				@Override
				protected void init() {
					directionWanted = new Direction(0, 1.0);

					this.collisionRadius = 25;
					this.height = 80;
					this.width = 80;
					double x = InGameData.getRandomInstance().nextInt(
							(int) InGameData.getInstance().getMap().getWidth());
					positionWanted = new Point2D(0 + x, height + 10
							- collisionRadius);
					this.speed = 80;
					this.dommage = 3;
					this.hitBoss = true;
					this.hitObject = true;
					this.hitChampion = true;
					this.hitPlayer = true;
					this.count = 1;

				}

				@Override
				public void collisionBehaviour(Object o) {
					super.collisionBehaviour(o);
				}

				@Override
				public void apparitionBehaviour() {

					InGame.getInstance().bulletCollision(this);

					GameRefreshManager.getInstance().store(this, this);

				}

				@Override
				public void refreshBehabiour(long time) {

					InGame.getInstance().bulletCollision(this);

				}

				@Override
				public void disparitionBehaviour() {

					GameRefreshManager.remove(this);

				}
			};

		}
		return b;
	}

	// /Images
	public static final HashMap<Type, Image> bulletImages = new HashMap<>();

	public static void loadImages() {
		Image i = null;
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\javelin.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.JAVELIN, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\mushroom.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.MUSHROOM, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\fireball.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.FIREBALL, i);
		bulletImages.put(Type.BIGFIREBALL, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\blueball.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.NRJ, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\chobullet.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.CHOSPIKE, i);

		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\laser.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.LASER, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\bluejavelin.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.NRJJAVELIN, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\atomicbombe.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.ATOMICROCKET, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\rocket.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.ROCKET, i);

		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\explosion.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.EXPLOSION, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\bushes.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.BUSH, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.PLAYER_PATH
					+ "\\RupturePower.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.RUPTURE, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.PLAYER_PATH
					+ "\\FeralScreamPower.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.FERIALSCREAM, i);
		try {
			i = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\bullet\\demancia.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bulletImages.put(Type.DEMANCIA, i);
	}

	public static Image getBulletImage(Type type) {

		return bulletImages.get(type);

	}

	public double getApparitionMinDistance() {

		return apparitionMinDistance;
	}

	public boolean isHitPlayer() {
		return hitPlayer;
	}

	public void setHitPlayer(boolean hitPlayer) {
		this.hitPlayer = hitPlayer;
	}

	public boolean isHitBoss() {
		return hitBoss;
	}

	public void setHitBoss(boolean hitBoss) {
		this.hitBoss = hitBoss;
	}

	public boolean isHitChampion() {
		return hitChampion;
	}

	public void setHitChampion(boolean hitChampion) {
		this.hitChampion = hitChampion;
	}

	public boolean isHitObject() {
		return hitObject;
	}

	public void setHitObject(boolean hitObject) {
		this.hitObject = hitObject;
	}

	public void setApparitionMinDistance(double dist) {

		apparitionMinDistance = dist;
	}

	public double getCollisionRadius() {
		return collisionRadius;
	}

	public void setCollisionRadius(double collisionRadius) {
		this.collisionRadius = collisionRadius;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Type getType() {
		return type;
	}

	public int getLifeTimeMS() {
		return lifeTimeMS;
	}

	public int getDommage() {
		return dommage;
	}

	public double getSpeed() {
		return speed;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction.joinData(direction);
	}

	@Override
	public String toString() {
		StringBuilder builder2 = new StringBuilder();
		builder2.append("Bullet [type=");
		builder2.append(type);
		builder2.append(", lifeTimeMS=");
		builder2.append(lifeTimeMS);
		builder2.append(", dommage=");
		builder2.append(dommage);
		builder2.append(", speed=");
		builder2.append(speed);
		builder2.append(", direction=");
		builder2.append(direction);
		builder2.append(", apparitionMinDistance=");
		builder2.append(apparitionMinDistance);
		builder2.append(", collisionRadius=");
		builder2.append(collisionRadius);
		builder2.append(", width=");
		builder2.append(width);
		builder2.append(", height=");
		builder2.append(height);
		builder2.append("]");
		return builder2.toString();
	}

	private static ImageViewBuilder<?> builder = ImageViewBuilder.create();

	public Node getNode() {
		Group g = new Group();

		if (this.type == Type.RUPTURE || this.type == Type.FERIALSCREAM) {
			PowerNode pn = PowerNode.getNode(this.collisionRadius,
					Bullet.getBulletImage(type));

			if (this.type == Type.FERIALSCREAM)
				pn.setRot(360 - Direction.getAngle(direction));

			g.getChildren().add(pn);
		} else {
			builder.fitHeight(this.height).fitWidth(this.width);
			Image i = bulletImages.get(this.getType());

			builder.image(i);
			ImageView l = builder.build();
			g.getChildren().add(l);

			if (this.type == Type.ROCKET || this.type == Type.JAVELIN
					|| this.type == Type.FIREBALL
					|| this.type == Type.BIGFIREBALL
					|| this.type == Type.CHOSPIKE || (this.type == Type.LASER)) {

				g.setRotate(360 - Direction.getAngle(direction));
			} else if (this.type == Type.DEMANCIA)
				g.setRotate(360 - Direction.getAngle(direction) - 90);

			if (this.type == Type.EXPLOSION)
				l.setOpacity(0.0);
			l.setLayoutX(-l.getBoundsInLocal().getWidth() / 2);
			l.setLayoutY(-l.getBoundsInLocal().getHeight() / 2);

		}
		return g;
	}

}
