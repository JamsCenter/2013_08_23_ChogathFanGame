package be.vgqd.vrjam.game.chogath.data.modele;

import java.util.Random;

import be.vgqd.vrjam.game.chogath.data.modele.Map.MapType;

public class InGameData {

	private static InGameData INSTANCE = new InGameData();

	private Map.MapType maptype;
	private Map map;
	private int ragequit,kill;

	private double difficulty;
	private boolean gameStarted;
	private boolean gameover;
	
	private Player player = new Player();

	private boolean pause;
	
	private static  Random r = new Random();
	public static Random getRandomInstance(){return r;}
	
	
	public InGameData() {


	}	

	public void setMap(MapType type) {
		if (type == null)
			maptype = MapType._5V5Mini;
		maptype = type;
		map = Map.getMap(maptype);;
	}

	public Map getMap() {
		return map;
	}


	public static InGameData getInstance() {
		return INSTANCE;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public Player getPlayer() {
		return player;
	}

	public double getDifficulty() {
		return difficulty;
	}
	public int getLevel() {
		return (int)(getDifficulty()/0.01);}
	

	public void addOneLevelOfDifficulty() {
		if (this.difficulty<1.0)
		this.difficulty += 0.01;
	
	}

	public boolean isGameStarted() {
		return gameStarted;
	}
	

	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public static void clean() {
		getInstance().setMap(Map.MapType._5V5Mini);
		getInstance().difficulty=0.0;
		getInstance().player.setLife(1);
		getInstance().ragequit=0;
		getInstance().kill=0;
		getInstance().pause=false;
		getInstance().gameover=false;
		
	}

	public void plusOneKill() {
		kill++;
		
	}

	public void plusRageQuit() {
		ragequit++;
	}

	public int getRagequit() {
		return ragequit;
	}



	public int getKill() {
		return kill;
	}

	public void setPause() {
		this.pause = !this.pause;
		
	}

	public boolean isPause() {
		return pause;
		
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	public boolean isGameover() {
		return gameover;
	}

	public void setGameover(boolean gameover) {
		this.gameover = gameover;
	}

private boolean ghostused;
	public boolean isGhostused() {
	return ghostused;
}


public void setGhostused(boolean ghostused) {
	this.ghostused = ghostused;
}



	
	
	
	
	
	
	


}
