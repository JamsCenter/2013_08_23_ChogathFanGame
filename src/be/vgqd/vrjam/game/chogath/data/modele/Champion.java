package be.vgqd.vrjam.game.chogath.data.modele;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import be.vgqd.vrjam.game.chogath.data.Paths;

/**
 * @author Eloi
 * 
 */
public class Champion {

	private String nom;
	private Image image;
	private Image imageProfil;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	
	public Image getImageProfil() {
		return imageProfil;
	}

	public void setImageProfil(Image imageProfil) {
		this.imageProfil = imageProfil;
	}

	private Champion(String nom, Image image) {
		if (nom == null || image == null)
			throw new IllegalArgumentException("The argument can't be null.");
		this.nom = nom;
		this.image = image;

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Champion [nom=");
		builder.append(nom);
		builder.append(", profil=");
		builder.append(image);
		builder.append("]");
		return builder.toString();
	}

	public static class NodeBuilder {
		private Champion champ = null;
		private static int width = 25, height = 25;

		private NodeBuilder() {
		}

		public static NodeBuilder create(Champion champion) {
			if (champion == null)
				throw new NullPointerException();
			NodeBuilder node = new NodeBuilder();
			node.champ = champion;

			return node;

		}

		public NodeBuilder width(int value) {
			width = value;
			return this;
		}

		public NodeBuilder height(int value) {
			height = value;
			return this;
		}

		public Node build() {
			ImageView img = new ImageView(champ.image);
			img.setFitHeight(height);
			img.setFitWidth(width);
			Group g= new Group();
			img.setLayoutX(-img.getFitWidth()/2);
			img.setLayoutY(-img.getFitHeight()/2);
			g.getChildren().add(img);
			return g;
		}

		public Champion getChamp() {
			return champ;
		}

		public void setChamp(Champion champ) {
			this.champ = champ;
		}

		public static int getWidth() {
			return width;
		}

		public static int getHeight() {
			return height;
		}

	}

	public static Champion getChampion(String nom) {
		return champions.get(nom);

	}

	public Node getNode() {
		return NodeBuilder.create(this).build();
	}

	public static Champion getChampion() {
		return getRandomChampion();
	}

	public static Champion getRandomChampion() {
		int rand = new Random().nextInt(championsName.size());

		return champions.get(championsName.get(rand));
	}

	private static final ArrayList<String> championsName = new ArrayList<>();
	private static final HashMap<String, Champion> champions = new HashMap<>();

	public static final void loadChampionData() {

		File championFolder = new File(Paths.IMAGECHAMPION_PATH);
		File championProfilFolder = new File(Paths.IMAGECHAMPION_PATH+"\\profil");

		if (championFolder.exists() && championFolder.isDirectory()) {
			try {

				Champion champ = null;
				String name = null;
				for (File f : championFolder.listFiles()) {
					if (f.isFile()) {
						name = f.getName().substring(0,
								f.getName().indexOf("."));
						champ = new Champion(name, new Image(
								new FileInputStream(f)));

						championsName.add(champ.nom);
						champions.put(champ.nom, champ);
						championslist.add(champ);
					}
				}
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}

		}
		
		if (championProfilFolder.exists() && championProfilFolder.isDirectory()) {
			try {

				Champion champ = null;
				String name = null;
				for (File f : championProfilFolder.listFiles()) {
					
					if (f.isFile()) {
						name = f.getName().substring(0,
								f.getName().indexOf("."));
						
						champ =champions.get(name);
						if (champ!=null)
							champ.imageProfil=new Image(
									new FileInputStream(f));
					}
				}
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}

		}


	}

	private static final List<Champion> championslist = new LinkedList<>();

	public static List<Champion> getChampionsInstanceList() {
		return championslist;
	}

	public static int getsize() {
		return champions.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Champion other = (Champion) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
	
	
}
