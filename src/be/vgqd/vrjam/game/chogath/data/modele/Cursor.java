package be.vgqd.vrjam.game.chogath.data.modele;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.CircleBuilder;

/**
 * @author Eloi
 *
 */
public class Cursor {

	
	
	private double x, y;

	private Cursor(double x, double y) {
		this.x =x;
		this.y =y;
	}
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}



	




	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cursor [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}








	public static class NodeBuilder {
		
		private static double x=0, y=0;
		private static double radius=2;
		
		private NodeBuilder (){}
		
		public static NodeBuilder create()
		{
			NodeBuilder node = new NodeBuilder();
			
			return node;
			
		}
		
		public NodeBuilder radius( double radius){NodeBuilder.radius=radius;return this;}
		public NodeBuilder x( double x){NodeBuilder.x=x;return this;}
		public NodeBuilder y( double y){NodeBuilder.y=y;return this;}

		public Node build()
		{
			return CircleBuilder.create().layoutX(x).layoutY(y).radius(radius).fill(Color.RED).build();
		}
		
	}

	public Node getNode(){
		return NodeBuilder.create().build();
	}
	public Node getNode(double x, double y,double radius){
		return NodeBuilder.create().x(x).y(y).radius(radius).build();
	}
	
	public static Cursor mouseCursorInstance = new Cursor(0,0);
	public static Cursor getMouseCursorInstance(){return mouseCursorInstance;}
	
	public static Cursor oculusCursorInstance = new Cursor(0,0);
	public static Cursor getOculusCursorInstance(){return oculusCursorInstance;}
	

}
