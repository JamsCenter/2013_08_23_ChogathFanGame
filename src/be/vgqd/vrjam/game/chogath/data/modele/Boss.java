package be.vgqd.vrjam.game.chogath.data.modele;

import java.util.Collection;
import java.util.HashMap;

import javafx.scene.Node;
import javafx.scene.image.Image;
import be.vgqd.vrjam.game.chogath.data.GameBossMoveManager;
import be.vgqd.vrjam.game.chogath.data.GameData;
import be.vgqd.vrjam.game.chogath.data.GamePopAndTimeManager;
import be.vgqd.vrjam.game.chogath.data.GameSoundManager;
import be.vgqd.vrjam.game.chogath.data.GameSoundManager.GameSound;
import be.vgqd.vrjam.game.chogath.data.InGame;
import be.vgqd.vrjam.game.chogath.data.interfaces.Refreshable;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet.Type;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.nodes.BossNode;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;

public class Boss implements Refreshable {

	protected int prefLevelToPop;
	protected boolean alive = true;
	protected boolean popped = false;
	protected boolean hide = false;
	protected Bullet.Type bullet;
	protected Champion champion;
	protected String name;
	protected String description;

	protected Image imageL;
	protected Image imageR;

	protected int hp = 10;
	protected int maxTimeInGameMS = 80000;

	protected int attackFrequenceMS = 1000;
	protected int specialEventAttackFrequenceMS = 8000;
	protected boolean melee;
	protected String apparitionMsg;
	protected String hideMsg;
	protected String deathMsg;
	protected static String defaultapparitionMsg="Hi, gl hf";
	protected static String defaulthideMsg="You so noob, I am going to hunt someone else";
	protected static String defaultdeathMsg="Cho'Gath is sSOoo .. OP";

	public Boss() {

		init();
	}

	public void init() {
		prefLevelToPop = InGameData.getRandomInstance().nextInt(90) + 9;
	}

	public void apparitionBehavoir() {
		InGame.getInstance().sendProvocation(this, apparitionMsg==null?defaultapparitionMsg:apparitionMsg);
	}

	public void loseLifeBehavoir() {
	}

	public void deathBehavoir() {

		InGame.getInstance().sendProvocation(this, deathMsg==null?defaultdeathMsg:deathMsg);
	}

	public void hideBehavoir() {
		InGame.getInstance().sendProvocation(this, hideMsg==null?defaulthideMsg:hideMsg);

		InGame.getInstance().askToRemove(this);
	}

	public void attackBehavoir() {

		if (this.bullet != null)
			if (popped && alive && !hide) {
				if (lastTime - lastTimeAttack > attackFrequenceMS
						) {
					lastTimeAttack = lastTime;

					Bullet b = Bullet.getBullet(bullet);
					b.setHitBoss(false);
					b.setHitChampion(false);
					b.setHitObject(false);
					b.setHitPlayer(true);
					InGame.getInstance().addBullet(b, this,
							InGameData.getInstance().getPlayer());

				}
			}
	}

	public void meleeAttackBehavoir() {
		if (melee)
			if (popped && alive && !hide) {
				if (lastTime - lastTimeMeleeAttack > attackFrequenceMS) {
					lastTimeMeleeAttack = lastTime;

					InGameData.getInstance().getPlayer().hit();
					GameSoundManager.play(GameSound.CHOGATH_HIT);
					InGame.getInstance().checkPlayerState();
				}
			}

	}

	public void specialEventAttackBehavoir() {

		if (popped && alive && !hide) {
			if (lastTime - lastTimeSpecialEvent > specialEventAttackFrequenceMS) {
				lastTimeSpecialEvent = lastTime;
			

			}
		}
	}

	protected long apparitionTime;
	protected long lastTime;
	protected long lastTimeAttack;
	protected long lastTimeMeleeAttack;
	protected long lastTimeSpecialEvent;
	private double bulletRandomPopFrequence;

	@Override
	public void refresh(long time) {
		if (apparitionTime <= 0)
			apparitionTime = time;
		lastTime = time;

		attackBehavoir();
		specialEventAttackBehavoir();

		if (maxTimeInGameMS < time - apparitionTime) {
			hide = true;
			hideBehavoir();
		}

	}

	private static HashMap<Champion, Boss> boss = new HashMap<Champion, Boss>();

	public static Boss getBoss(Champion champ) {
		return boss.get(champ);
	}

	public Boss(Champion ch, Bullet.Type bullet, Image left, Image right) {
		if (ch == null || bullet == null)
			throw new NullPointerException();
		champion = ch;
		this.bullet = bullet;
		name = champion.getNom();
		description = "None";
		alive = true;
		popped = false;
		imageL = left;
		imageR = right;
		bulletRandomPopFrequence = GameData.POP_BULLET_TIME;
	}

	public static void loadBoss() {
		Champion ch = null;
		Bullet.Type bullet = null;
		Boss boss = null;

		bullet = Bullet.Type.MUSHROOM;
		ch = Champion.getChampion("Teemo");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.apparitionMsg="Teemo, ready for duty !";
		boss.hideMsg="Too easy, I go shroom other player";
		boss.deathMsg="All that for 2 kg of meat... Puff";
		boss.prefLevelToPop = 2;// 2;
		boss.attackFrequenceMS = 2000;
		boss.bulletRandomPopFrequence = 4000;
		boss.description = "Teemo will put mushroom next to him util you kill him.";
		Boss.boss.put(ch, boss);

		bullet = Bullet.Type.JAVELIN;
		ch = Champion.getChampion("Nidalee");
	
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.apparitionMsg="The jungle is mine, be readty to be hunted.";
		boss.hideMsg="Did you know that I was there ?";
		boss.deathMsg="Nooo my jungle !!!";
		boss.prefLevelToPop = 9;
		boss.description = "Nidalee throw some big javelin that hurt a lot";
		Boss.boss.put(ch, boss);

		bullet = Bullet.Type.CHOSPIKE;
		ch = Champion.getChampion("Chogath");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.apparitionMsg="Dad is home ! Come see daddy.";
		boss.hideMsg="Enought of eating you, going to taste someone else.";
		boss.deathMsg="Noooooo, next time your turn to die.";

		
		boss.attackFrequenceMS = 3000;
		boss.bulletRandomPopFrequence = 5000;
		boss.prefLevelToPop = 16;
		boss.melee = true;
		boss.description = "Be worry of the Chogath, he can throw spikes around him and eat you if you stay next to him more then 2 secondes";
		Boss.boss.put(ch, boss);

		bullet = Bullet.Type.NRJ;
		ch = Champion.getChampion("Xerath");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.apparitionMsg="The energy is going to rule this league ... Bzbz bzz bzzzz";
		boss.hideMsg="There is not enought energy here.";
		boss.prefLevelToPop = 22;
		boss.bulletRandomPopFrequence = 3000;
		boss.description = "Xerath attaks by sending lot of energy balls in all direction, try to dodge them";
		Boss.boss.put(ch, boss);

		bullet = Bullet.Type.LASER;
		ch = Champion.getChampion("Lux");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.apparitionMsg="Time to illumate this map by my lighting fires.";
		boss.hideMsg="With all that light, you are not able to see me ?!?";
		boss.deathMsg="And the darkness will be.";
		boss.prefLevelToPop = 30;
		boss.attackFrequenceMS = 500;
		boss.bulletRandomPopFrequence = 2000;
		boss.description = "Do you want to know how to suffer, stop moving.";
		Boss.boss.put(ch, boss);

		bullet = Bullet.Type.BIGFIREBALL;
		ch = Champion.getChampion("Shyvana");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.prefLevelToPop = 37;
		boss.apparitionMsg="A dragon, I saw a dragon ! Oh no it just a Cho'Gath... Come here !";
		boss.hideMsg="That time it is a dragon ! Nothing to do here.";
		boss.deathMsg="You maybe not a dragon, but you are as dangerous as them.";
		boss.attackFrequenceMS = 3000;
		boss.bulletRandomPopFrequence = 4000;
		boss.description = "He is a dragon killer !";
		Boss.boss.put(ch, boss);
		
		bullet = Type.DEMANCIA;
		ch = Champion.getChampion("Garen");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage()) {
			@Override
			public void apparitionBehavoir() {
				InGame.getInstance().addBullet(Type.BUSH);
				
				GUI.get(this).setLayout(
						InGameData.getInstance().getMap().getCenterX(),
						InGameData.getInstance().getMap().getCenterY());
				GameBossMoveManager.getInstance().stop(this, true);
			}

			
			@Override
			public void attackBehavoir() {
			if(	GameBossMoveManager.getInstance().isMoving(this	))
				super.attackBehavoir();
			}


			@Override
			public void hit() {
				GameBossMoveManager.getInstance().stop(this, false);
				InGame.getInstance().addBullet(Type.DEMANCIA);
				super.hit();
			}

			
		
		};
		boss.apparitionMsg="DEMANCIA. It is time to spine to win guys.";
		boss.hideMsg="I will spine away. You are just to fast for me.";
		boss.deathMsg="Next time you will s*** my sword Cho.";
		boss.bulletRandomPopFrequence=40000;
		boss.attackFrequenceMS = 10000;
		boss.prefLevelToPop = 45;
	
		boss.melee = true;
		boss.description = "Oh a bush, what is it inside ?";
		Boss.boss.put(ch, boss);
		bullet = Bullet.Type.FIREBALL;
		ch = Champion.getChampion("Annie");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.prefLevelToPop = 55;
		boss.attackFrequenceMS = 4000;
		boss.apparitionMsg="Did you see my Tiber ?";
		boss.hideMsg="Apparently my Tiber is not here...";
		boss.deathMsg="Tiber !!! where are you ...";

		boss.bulletRandomPopFrequence = 16000;
		boss.description = "Do you know where is his Tiber ?";
		Boss.boss.put(ch, boss);
		bullet = Bullet.Type.ROCKET;
		ch = Champion.getChampion("Corki");
		// changer l'image par une image de boss
		boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		boss.prefLevelToPop = 65;
		boss.apparitionMsg="All the area will burn like hell. And you with it";
		boss.hideMsg="My people need be, bye !";
		boss.deathMsg="Maydey, maydey !";
		boss.description = "You do not want to be next its rockets";
		Boss.boss.put(ch, boss);
		
		// bullet = Bullet.Type.REQUIEM;
		// ch = Champion.getChampion("Karthus");
		// // changer l'image par une image de boss
		// boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		// boss.prefLevelToPop = 80;
		// boss.description =
		// "You should kill him quick because you can dodge the death";
		// Boss.boss.put(ch, boss);
		// ch = Champion.getChampion("Tryndamere");
		// // changer l'image par une image de boss
		// boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		// boss.prefLevelToPop = 90;
		// boss.melee=true;
		// boss.description = "Oh boy run !!!";
		// Boss.boss.put(ch, boss);

		//bullet = Bullet.Type.PACMANBALL;
		//ch = Champion.getChampion("Pacman");
		// changer l'image par une image de boss
		// boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		// boss.prefLevelToPop = 90;
		// boss.description = "Miam Miam Miam, a total rival";
		// Boss.boss.put(ch, boss);

		// bullet =null;
		// ch = Champion.getChampion("ChuckNorris");
		// // changer l'image par une image de boss
		// boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		// boss.prefLevelToPop = 100;
		// boss.description =
		// "We are doooom !!! Try to stay alive, Game over and Thanks for playing";
		// Boss.boss.put(ch, boss);

		// bullet = Bullet.Type.CREEPERBALL;
		// ch = Champion.getChampion("Creeper");
		// // changer l'image par une image de boss
		// boss = new Boss(ch, bullet, ch.getImage(), ch.getImage());
		// boss.description = "THHHSSSSHSHHSHSHSHSHSHHSHSHSHHS Boom";
		// Boss.boss.put(ch, boss);
	}

	public Node getNode() {
		BossNode bn = BossNode.getNode(champion.getImage(), this.hp);
		bn.setScaleX(1.4);
		bn.setScaleY(1.4);
		return bn;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getPrefLevelToPop() {
		return prefLevelToPop;
	}

	public void setPrefLevelToPop(int prefLevelToPop) {
		this.prefLevelToPop = prefLevelToPop;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
		if (!alive)
			deathBehavoir();
	}

	public boolean isPopped() {
		return popped;
	}

	public void setPopped(boolean popped) {
		this.popped = popped;
		if (this.popped)
			apparitionBehavoir();
	}

	public Bullet.Type getBullet() {
		return bullet;
	}

	public void setBullet(Bullet.Type bullet) {
		this.bullet = bullet;
	}

	public Champion getChampion() {
		return champion;
	}

	public void setChampion(Champion champion) {
		this.champion = champion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Image getImageL() {
		return imageL;
	}

	public void setImageL(Image imageL) {
		this.imageL = imageL;
	}

	public Image getImageR() {
		return imageR;
	}

	public void setImageR(Image imageR) {
		this.imageR = imageR;
	}

	public static Collection<Boss> values() {

		return boss.values();
	}

	public boolean isMelee() {
		return melee;
	}

	public void setMelee(boolean melee) {
		this.melee = melee;
	}

	protected long lastTimeHit;

	public void hit() {
		long now = GamePopAndTimeManager.getInstance().getTimePlayed();
		if (now - lastTimeHit > 300) {
			lastTimeHit=now;
			if (hp > 0)
				this.hp--;
			if (hp <= 0)
				this.alive = false;

			loseLifeBehavoir();

			// replacement du boss
			StereoNode<Node> sn = GUI.get(this);
			if (sn != null) {
				BossNode bn = null;
				if (sn.getLeft() instanceof BossNode) {
					bn = (BossNode) sn.getLeft();
					bn.setText(hp);
				}
				if (sn.getRight() instanceof BossNode) {
					bn = (BossNode) sn.getRight();
					bn.setText(hp);
				}

			}

		}
	}

	public static void clean() {

		boss.clear();
		loadBoss();

	}

	public boolean isHide() {
		return hide;
	}

	public double getBulletRandomPopFrequenceTime() {
		return bulletRandomPopFrequence;
	}

	public static Boss getBoss(String v) {
		return getBoss(Champion.getChampion(v));
	}

}
