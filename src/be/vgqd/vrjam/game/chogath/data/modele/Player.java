package be.vgqd.vrjam.game.chogath.data.modele;



import be.vgqd.vrjam.game.chogath.data.GameData;

public class Player {

	private int life = 1;
	private int speed = GameData.PLAYER_SPEED;
	private boolean dommageImmune;


	
	
	
	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		if (life > GameData.MAXPLAYERLIFE)
			life = GameData.MAXPLAYERLIFE;
		if (life < 0)
			life = 0;
		this.life = life;
	}

	public void hit() {
		setLife(life - 1);

	}
	public void setShield (boolean on){this.dommageImmune=on;}
	public void hit(int dommage)
	{
		if (!dommageImmune)
		setLife(life-dommage);
		
		
	}

	public void miamMiam() {
		setLife(life + 1);

	}

	public void setSpeed(int playerSpeed) {
		speed = playerSpeed;

	}

	public int getSpeed() {
		return speed;
	}

	public Champion getChampion() {
		return Champion.getChampion("Chogath");
	}

	public boolean isShieldOn() {
		
		return dommageImmune;
	}
}
