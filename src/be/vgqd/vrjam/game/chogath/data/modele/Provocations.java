package be.vgqd.vrjam.game.chogath.data.modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import be.vgqd.vrjam.game.chogath.data.Paths;
import be.vgqd.vrjam.game.chogath.data.modele.Provocations.Provocation.Type;

public class Provocations {

	private static Provocations INSTANCE = new Provocations();

	public static Provocations getInstance() {
		return INSTANCE;
	}

	HashMap<Integer, Provocation> provocations = new HashMap<Integer, Provocations.Provocation>();

	private Provocations() {
	}

	private void store(Provocation provoc) {
		if (provoc != null)
			provocations.put(
					Provocation.hashCode(provoc.hunter, provoc.hunted), provoc);

	}

	private static void add(Champion hunter, Champion hunted, Type type,
			String[] copyOfRange) {

		Provocation p = INSTANCE.provocations.get(Provocation.hashCode(hunter,
				hunted));
		if (p == null) {
			p = new Provocation(hunter, hunted);
		}
		for (String s : copyOfRange) {
			p.store(type, s);
		}
		INSTANCE.store(p);


	}

	/**
	 * @author Eloi
	 * 
	 */
	public static class Provocation {
		public enum Type {
			KILL, HIT, RAGEQUIT
		}

		private List<String> kill;
		private List<String> hit;
		private List<String> ragequit;
		public final Champion hunter;
		public final Champion hunted;

		public Provocation(Champion hunter, Champion hunted) {

			this.hunted = hunted;
			this.hunter = hunter;
		}

		public String getRandomProvocation(Provocation.Type type) {
			if (type != null) {

				if (type == Type.KILL && kill != null && kill.size() > 0) {
					Collections.shuffle(kill);
					return kill.get(0);
				} else if (type == Type.HIT && hit != null && hit.size() > 0) {
					Collections.shuffle(hit);
					return hit.get(0);
				} else if (type == Type.RAGEQUIT && ragequit != null
						&& ragequit.size() > 0) {
					Collections.shuffle(ragequit);
					return ragequit.get(0);
				}
			}
			return null;
		}

		public void store(Provocation.Type type, String value) {
			if (value != null && type != null && value.length() > 0) {
				if (type == Type.KILL) {
					if (kill == null)
						kill = new ArrayList<String>();
					kill.add(value);
				} else if (type == Type.HIT) {
					if (hit == null)
						hit = new ArrayList<String>();
					hit.add(value);
				} else if (type == Type.RAGEQUIT) {
					if (ragequit == null)
						ragequit = new ArrayList<String>();
					ragequit.add(value);
				}
			}
		}

		public static int hashCode(Champion hunted, Champion hunter) {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((hunted == null) ? 0 : hunted.hashCode());
			result = prime * result
					+ ((hunter == null) ? 0 : hunter.hashCode());
			return result;

		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Provocation other = (Provocation) obj;
			if (hunted == null) {
				if (other.hunted != null)
					return false;
			} else if (!hunted.equals(other.hunted))
				return false;
			if (hunter == null) {
				if (other.hunter != null)
					return false;
			} else if (!hunter.equals(other.hunter))
				return false;
			return true;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Hunter: ");
			builder.append(hunter);
			builder.append("\t Hunted:");
			builder.append(hunted);
			builder.append("\n Rage:");
			if (ragequit != null)
				for (String s : ragequit) {
					builder.append("\n");
					builder.append(s);

				}
			builder.append("\n Hit:");
			if (hit != null)
				for (String s : hit) {
					builder.append("\n");
					builder.append(s);

				}
			builder.append("\n Kill:");
			if (kill != null)
				for (String s : kill) {
					builder.append("\n");
					builder.append(s);

				}
			return builder.toString();
		}

	}

	public static void loadProvocations() {
		Scanner sc = null;
		try {

			File f = new File(Paths.PROVOCATION_PATH + "\\provocations.txt");
			sc = new Scanner(f);

			String[] tab = null;
			Provocation.Type type = null;
			Champion hunter = null;
			Champion hunted = null;

			while (sc.hasNextLine()) {

				tab = sc.nextLine().split(";");

				if (tab.length > 3) {
					if (tab[2].equals("HIT"))
						type = Type.HIT;
					else if (tab[2].equals("RAGEQUIT"))
						type = Type.RAGEQUIT;
					else if (tab[2].equals("KILL"))
						type = Type.KILL;
					else
						continue;
					hunter = Champion.getChampion(tab[0]);
					hunted = Champion.getChampion(tab[1]);

					Provocations.add(hunter, hunted, type,
							Arrays.copyOfRange(tab, 3, tab.length));
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			sc.close();
		}

	}

	public String getProvocation(Champion ch, Champion champion, Type type) {
		
		return "Fuck";
	}

}
