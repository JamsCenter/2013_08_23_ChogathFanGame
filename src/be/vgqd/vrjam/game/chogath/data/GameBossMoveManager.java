package be.vgqd.vrjam.game.chogath.data;

import java.util.HashMap;
import java.util.Random;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

import javafx.scene.Node;
import be.vgqd.vrjam.game.chogath.data.interfaces.Refreshable;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.game.TimeManager.TimerListener;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.utils.Direction;

public class GameBossMoveManager implements TimerListener {

	static final private GameBossMoveManager INSTANCE = new GameBossMoveManager();

	public static GameBossMoveManager getInstance() {
		return INSTANCE;
	}

	private final HashMap<Object, BossMove> bossMoving = new HashMap<>();

	public static void remove(Object o) {
		getInstance().bossMoving.remove(o);
	}

	public void store(Boss o) {
		if (o != null && bossMoving.get(o) == null) {
			BossMove bm = new BossMove(o);
			bossMoving.put(o, bm);
			bm.refresh(GamePopAndTimeManager.getInstance().getTimePlayed());
		}
	}

	public Direction getDirectionOf(Boss boss) {
		BossMove m = bossMoving.get(boss);
		return m != null ? m.direction : null;
	}

	@Override
	public void ticTac() {
		final long time = GamePopAndTimeManager.getInstance().getTimePlayed();

		for (BossMove r : bossMoving.values()) {

			r.refresh(time);
		}

	}

	public static void clean() {
		getInstance().bossMoving.clear();

	}

	private static Random random = new Random();

	public static class BossMove implements Refreshable {
		private BossMove(Boss b) {
			this.boss = b;
			if (b.isMelee()) {
				maxTimeNextChangeMS = 20;
			}
		}

		private final Boss boss;
		private final Direction direction = new Direction(0, 0);
		private long lastTimeChangedDirection = 0;
		private int nextChange = 500;
		private int maxTimeNextChangeMS = 15000;
		private boolean stop;

		@Override
		public void refresh(long time) {
			if (stop) {
				direction.setX(0.0);
				direction.setY(0.0);
			} else {
				if (lastTimeChangedDirection == 0)
					lastTimeChangedDirection = time;

				if (time - lastTimeChangedDirection > nextChange) {

					nextChange = random.nextInt(maxTimeNextChangeMS);
					lastTimeChangedDirection = time;

					if (boss.isMelee()) {
						StereoNode<Node> sn = GUI.get(boss);
						if (sn != null)
							Direction.setDirection(
									direction,
									InGame.getInstance().getPlayerDirection(
											sn.getLayoutX(), sn.getLayoutY()));

					} else {
						Direction.setDirectionOnRandom(direction, random);
					}
				}
			}
		}

	}

	public void stop(Boss boss, boolean stopMoving) {

		if (boss != null)
			bossMoving.get(boss).stop = stopMoving;
	}

	public boolean isMoving(Boss boss) {
		if (boss != null) {
			BossMove mo = bossMoving.get(boss);
			if (mo != null)
				return !mo.stop;
		}
		return false;
	}

}
