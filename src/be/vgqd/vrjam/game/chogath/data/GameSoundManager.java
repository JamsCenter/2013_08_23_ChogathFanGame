package be.vgqd.vrjam.game.chogath.data;

import be.vgqd.vrjam.game.chogath.game.sounds.Sound;
import be.vgqd.vrjam.game.chogath.game.sounds.Sound.SoundType;

public class GameSoundManager {

	public enum GameSound {
		PING, CHOGATH_HIT,CHOGATH_MIAM, TEEMO_PUT_MUSHROOM, MUSHROOM_DESTROYED, CHAMPION_APPARITION,NIDALEE_JAVELIN_HIT, NIDALEE_JAVELIN_APPEARTED, BOSS_APPARITION
	}
	public enum GameMusic {
		HOMEBACKGROUND,INGAMEBACKGROUND,VICTORYBACKGROUND,GAMEOVERBACKGROUND
	}
	
	public static void play(GameSound sound)
	{
		Sound.play(getPath(sound), SoundType.SOUND);
	}
	
	public static void play(GameMusic sound)
	{

		Sound.play(getPath(sound), SoundType.AMBIANCE);
	}
	
	private static String getPath(final GameSound sound)
	{	
		switch (sound) {
		case CHOGATH_MIAM:
			return "miamMiam.mp3";
		case CHOGATH_HIT:
			return "ouch.wav";
		default:
			return null;
		}
	}
	private static String getPath(final GameMusic sound)
	{	
		switch (sound) {
		case HOMEBACKGROUND:
			return "homeSound.mp4";
		case INGAMEBACKGROUND:
			return "backGroundSound.wav";
		default:
			return null;
		}
	}


}
