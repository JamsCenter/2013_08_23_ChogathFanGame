package be.vgqd.vrjam.game.chogath.data;


/** Contient toutes les donn�es sur le jeux video */
public class GameData {

	// Fix data
	public static final int MAXSPEED = 300;
	public static final int MAXPLAYERLIFE = 6;
	public static final int RAGEQUITNUMBER = 3;

	public static final int PLAYER_SPEED =400;
	public static final int PLAYER_SPEED_WITH_GHOST =600;
	
	public static final int FPS_REFRESH_RECORDER= 8;
	public static final int FPS_REFRESH_IMPORTAN_OBJECT= 30;
	public static final double DEFAUTSPEED = 100;//px/s
	
	public static final double PCT_HITBOX_RADIUS_JAVELIN=1.0;
	public static final double PCT_HITBOX_RADIUS_PLAYER=0.6;
	public static final double PCT_HITBOX_RADIUS_MUSHROOM=0.5;
	public static final double PCT_HITBOX_RADIUS_BOSS = 1.1;

	
	public static final double POP_MUSHROOM_TIME = 4000;//m/s
	public static final double POP_BULLET_TIME = 3000;//m/s
	public static final double POP_CHAMPION_TIME = 2000;//m/s

	public static final double TIME_DIFFICULTY_LEVELUP = 10000;//20000;//m/s

	
	public static final double JAVELIN_SPEED = 100;

	// INSTANCE
	public static GameData INSTANCE = new GameData();

	// DATA
	private double sound=1.0;
	//private Provocations provoque;
	
	

	public GameData()
	{
		clean();
		
		
	}
	
	
	
	public static double getSound() {
		if (INSTANCE==null)return 1.0;
		return INSTANCE.sound;
	}

	public static void setSound(double sound) {
		if (INSTANCE!=null)
		if (INSTANCE.sound <= 0.)
			INSTANCE.sound = 0;
		else if (INSTANCE.sound >= 1.)
			INSTANCE.sound = 1;
		else
			INSTANCE.sound = sound;
	}



	public static void clean() {
		setSound(1);
		
	}

}
