package be.vgqd.vrjam.game.chogath.nodes;

import java.io.File;
import java.io.FileInputStream;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;
import be.vgqd.vrjam.game.chogath.data.Paths;

public class PlayerNode extends Pane {
	
	private double width=20,height=20;
	private ImageView image= ImageViewBuilder.create().fitHeight(height).fitWidth(width).build();
	private Circle state;
	private Circle shield;
	private boolean shieldActivated;
	public static final double RADIUS=10;
	private static	CircleBuilder<?> cb = CircleBuilder.create().opacity(0.5).fill(Color.DARKRED);

	private Group g = new Group();
	public PlayerNode()
	{
		try{
		Image img = new Image(new FileInputStream(new File(Paths.PLAYER_PATH+"\\cho.png")));
		image.setImage(img);

		
		}catch(Exception e){e.printStackTrace();} 
		
		cb.radius(RADIUS);
		state = cb.layoutX(width/2).layoutY(height/2).build();
		shield= cb.fill(Color.LIGHTGRAY).radius(RADIUS+2).build();
		setShield(false);
		setLife(1);
		
		g.getChildren().addAll(shield,state,image);
		g.setLayoutX(-g.getBoundsInLocal().getWidth()/2);
		g.setLayoutY(-g.getBoundsInLocal().getHeight()/2);
		getChildren().add(g);
	}
	
	public void setLife (int i )
	{	double scale = 1.0+(double)i*2.0/10.0;
		g.setScaleX(scale);
		g.setScaleY(scale);
			if (i > 5) {
				state.setFill(Color.GREEN);
			} else if (i > 4) {
				state.setFill(Color.YELLOWGREEN);
			} else if (i > 3) {
				state.setFill(Color.YELLOW);
			} else if (i > 2) {
				state.setFill(Color.ORANGE);
			} else if (i > 1) {
				state.setFill(Color.RED);
			} else {

				state.setFill(Color.DARKRED);
			}
		
		
		
	}
	public void setShield(boolean on)
	{
		shieldActivated=on;
		shield.setVisible(shieldActivated);
	}
	
	public static PlayerNode getNode()
	{return new PlayerNode();}
}

