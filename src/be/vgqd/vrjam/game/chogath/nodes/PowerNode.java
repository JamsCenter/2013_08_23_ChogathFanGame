package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;

public class PowerNode extends Pane{

	private double width=70,height=70;
	private ImageView image= ImageViewBuilder.create().fitHeight(height).fitWidth(width).build();
	private Circle state;
	private static	CircleBuilder<?> cb = CircleBuilder.create().visible(false).opacity(0.0).fill(Color.DARKRED);

	private Group g = new Group();
	public PowerNode( double radius,Image img)
	{
		height=width= radius*2;
		image.setFitWidth(width);
		image.setFitHeight(height);
			image.setImage(img);

		
		cb.radius(radius);
		state = cb.layoutX(width/2).layoutY(height/2).build();

		g.getChildren().addAll(state,image);
		g.setLayoutX(-g.getBoundsInLocal().getWidth()/2);
		g.setLayoutY(-g.getBoundsInLocal().getHeight()/2);
		getChildren().add(g);
	}
	
	
	
	public static PowerNode getNode( double radius,Image img)
	{return new PowerNode(radius,img);}



	public void setRot(double d) {
		image.setRotate(d);
		
	}
	
	
}
