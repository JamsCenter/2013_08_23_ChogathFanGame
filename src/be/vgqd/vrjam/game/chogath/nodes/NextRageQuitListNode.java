package be.vgqd.vrjam.game.chogath.nodes;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import be.vgqd.vrjam.game.chogath.data.GameData;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;

public class NextRageQuitListNode extends VBox {

	private ChampionImageViewer[] championImageViewers = new ChampionImageViewer[5];

	public NextRageQuitListNode() {
		Rectangle rect = new Rectangle();
		rect.setStroke(Color.LIGHTGREEN);
		rect.setFill(Color.BLACK);

		championImageViewers[0] = new ChampionImageViewer(Champion.getChampion(
				"Nidalee").getImage());
		championImageViewers[1] = new ChampionImageViewer(Champion.getChampion(
				"Teemo").getImage());
		championImageViewers[2] = new ChampionImageViewer(Champion.getChampion(
				"Corki").getImage());
		championImageViewers[3] = new ChampionImageViewer(Champion.getChampion(
				"Lux").getImage());
		championImageViewers[4] = new ChampionImageViewer(Champion.getChampion(
				"Garen").getImage());
		int padding = 5;
		for (int i = 0; i < championImageViewers.length; i++) {

			championImageViewers[i].setPadding(new Insets(padding));
			this.getChildren().add(championImageViewers[i]);
		}
		rect.setWidth(this.getBoundsInLocal().getWidth());
		rect.setHeight(this.getBoundsInLocal().getHeight());
	}

	public void setText(Champion champ, int value) {

		ChampionImageViewer tmp = null;
		ChampionImageViewer cursor =null;
		for (int i = 0; i < championImageViewers.length; i++) {
			tmp = championImageViewers[i];
	
			if (tmp.getDeath() < value ||tmp.getDeath()>=GameData.RAGEQUITNUMBER) {
				cursor =tmp;
				break;
				
			}
		}
		if (cursor!=null){
			cursor.setImage(champ.getImage());
			cursor.setText(value);
		}
		else
		{

			Image tmpImg =null;
			int tmpDeath =0;
			for (int i = championImageViewers.length-1; i > 0; i--) {

				if(championImageViewers[i].getDeath()<championImageViewers[i-1].getDeath())
					tmpImg=championImageViewers[i].getImage();
				tmpDeath = championImageViewers[i].getDeath();
					championImageViewers[i].setImage(championImageViewers[i-1].getImage());
					championImageViewers[i].setText(championImageViewers[i-1].getDeath());
					
					championImageViewers[i-1].setImage(tmpImg);
					championImageViewers[i-1].setText(tmpDeath);
			}
			championImageViewers[0].setImage(champ.getImage());
			championImageViewers[0].setText(value);
				
			
			
		}
		

	}
	public static  NextRageQuitListNode getNode()
	{
		return new NextRageQuitListNode();
	}

}