package be.vgqd.vrjam.game.chogath.nodes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

import javafx.scene.image.Image;
import be.vgqd.vrjam.game.chogath.data.Paths;

public class TimeNode extends InGameDataPanel {

	static private Image img;
	static private SimpleDateFormat ft = 
  	      new SimpleDateFormat ("mm:ss");

	
	private TimeNode(Image i) {
		super(i);

	}

	public static TimeNode  getNode()
	{
		if (img == null) {
			try {
				img = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
						+ "\\component\\img_time.png")));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
		return new TimeNode(img);
		
	}

	@Override
	public void setText(long value) {
		  

		
		t.setText(ft.format(value));
		imagev.setLayoutX(t.getBoundsInLocal().getWidth());
	}
	
	

}
