package be.vgqd.vrjam.game.chogath.nodes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import be.vgqd.vrjam.game.chogath.data.Paths;

public class RageQuitNode extends InGameDataPanel {

	static private Image img;

	private HBox g;
	private FadeTransition fadeTransition;
	private boolean withText = false;
	private Text text;
	private ImageView champImage = ImageViewBuilder.create().opacity(0.7)
			.fitHeight(15).fitWidth(15).build();

	private RageQuitNode(Image i, boolean withtext) {
		super(i);
		this.withText = withtext;
		if (withtext) {
			g = new HBox();
			fadeTransition = FadeTransitionBuilder.create()
					.duration(Duration.seconds(5)).node(g).fromValue(8.0)
					.toValue(0.0).onFinished(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {

							fadeTransition.stop();
						}
					}).build();
			text = Texts.getTextbuilder().text("has deconnected").build();
			g.setOpacity(0.0);
			g.getChildren().addAll(champImage, text);
			this.getChildren().add(g);

		}

	}

	public void setText(Image champdeconnected, long value) {
		// TODO Auto-generated method stub
		super.setText(value);
		if (withText) {
			champImage.setImage(champdeconnected);
			fadeTransition.playFromStart();
		}
	}

	public static RageQuitNode getNode(boolean withText) {
		if (img == null) {
			try {
				img = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
						+ "\\component\\img_ragequit.png")));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
		return new RageQuitNode(img,withText);

	}

}
