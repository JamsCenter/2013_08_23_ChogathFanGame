package be.vgqd.vrjam.game.chogath.nodes;

import java.util.HashMap;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import be.vgqd.vrjam.game.chogath.data.modele.Power;
import be.vgqd.vrjam.game.chogath.data.modele.Power.Type;

public class PowerAndCapacityNode extends VBox {

	private HashMap<Power.Type, PowerNode> powers = new HashMap<>();

	public PowerAndCapacityNode() {
		PowerNode p = null;
		p = new PowerNode(Type.CAPACITY1, 5);
		powers.put(Type.CAPACITY1, p);
		this.getChildren().add(p);
	
		p = new PowerNode(Type.CAPACITY2, 5);
		powers.put(Type.CAPACITY2, p);
		this.getChildren().add(p);
		Rectangle empty = new Rectangle();
		empty.setWidth(10);
		empty.setHeight(20);
		empty.setVisible(false);
		this.getChildren().add(empty);
		p = new PowerNode(Type.POWER1, 5);
		powers.put(Type.POWER1, p);
		this.getChildren().add(p);
		p = new PowerNode(Type.POWER2, 5);
		powers.put(Type.POWER2, p);
		this.getChildren().add(p);
		p = new PowerNode(Type.POWER3, 5);
		powers.put(Type.POWER3, p);
//		this.getChildren().add(p);
		p = new PowerNode(Type.POWER4, 5);
		powers.put(Type.POWER4, p);
		this.getChildren().add(p);

	}

	public void set(Power.Type type, double complet) {

		PowerNode p = powers.get(type);
		p.set(complet);
	}

	public static class PowerNode extends Pane {
		Node image;
		Rectangle rect;

		public PowerNode(Power.Type type, int padding) {

			Power p = Power.getPower(type);
			image = p.getNode();
			image.setLayoutX(padding);
			image.setLayoutY(padding);
			double width = image.getBoundsInLocal().getWidth();
			double height = image.getBoundsInLocal().getHeight();
			this.setWidth(width + 2 * padding);
			this.setHeight(height + 2 * padding);

			rect = new Rectangle();
			rect.setWidth(width);
			rect.setHeight(height);
			rect.setLayoutX(padding);
			rect.setLayoutY(padding);
			rect.setOpacity(0.0);
			rect.setFill(Color.TRANSPARENT);
			rect.setStrokeWidth(1.2);
			this.getChildren().addAll(image, rect);

		}

		public void set(double complet) {
			if (complet >0);
			image.setOpacity(complet+0.3);
			if (complet >=0 &&complet < 0.8) {
				rect.setStroke(Color.ORANGE);
			
				rect.setOpacity(complet);

			} else if (complet >=0 &&complet < 0.95) {
				rect.setStroke(Color.YELLOW);
				rect.setOpacity(complet);
			} else {
				rect.setStroke(Color.GREEN);
				rect.setOpacity(1.0);

			}

		}
	}

	public static PowerAndCapacityNode getNode() {
		return new PowerAndCapacityNode();
	}

}
