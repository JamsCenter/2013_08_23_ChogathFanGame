package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ChampionImageViewer extends Pane {

	private static double imageWidth = 25, imageheight = 25;
	private int death = -1;
	private ImageView championImage = new ImageView();
	private Rectangle rectquit = new Rectangle();
	private Rectangle rectstate = new Rectangle();

	public ChampionImageViewer(Image img) {
		if (img == null)
			throw new IllegalArgumentException();
		rectquit.setHeight(imageheight);
		rectquit.setWidth(imageWidth);
		rectquit.setFill(Color.LIGHTGREEN);
		rectquit.setOpacity(0.7);
		rectquit.setVisible(false);
		
		rectstate.setHeight(imageheight);
		rectstate.setWidth(imageWidth);
		rectstate.setFill(Color.TRANSPARENT);
	
		championImage.setImage(img);
		championImage.setFitHeight(imageheight);
		championImage.setFitWidth(imageWidth);
		setText(-1);
		this.getChildren().addAll(championImage,rectstate, rectquit);

	}

	public void setText(int value) {
		death = value;
		rectquit.setVisible(false);
		if (death == -2) {
			rectquit.setVisible(true);
		} else if (death == -1) {
			this.setOpacity(0.4);


		} else {

			this.setOpacity(1.0);
			switch (value) {
			case 1:
				rectstate.setStroke(Color.GREEN);
				break;
			case 2:
				rectstate.setStroke(Color.YELLOW);

				break;
			case 3:
				rectstate.setStroke(Color.RED);

				break;
				default:
				rectstate.setFill(Color.TRANSPARENT);
			}
		}


	}

	public static double getImageWidth() {
		return imageWidth;
	}

	public static void setImageWidth(double imageWidth) {
		ChampionImageViewer.imageWidth = imageWidth;
	}

	public static double getImageHeight() {
		return imageheight;
	}

	public static void setImageHeight(double imageheight) {
		ChampionImageViewer.imageheight = imageheight;
	}

	public int getDeath() {
		return death;
	}

	public void setImage(Image img) {
		championImage.setImage(img);

	}

	public Image getImage() {
		return championImage.getImage();
	}

}
