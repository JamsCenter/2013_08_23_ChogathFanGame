package be.vgqd.vrjam.game.chogath.nodes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;
import be.vgqd.vrjam.game.chogath.data.Paths;

public class KillNode extends InGameDataPanel {

	static private Image img;
	

	
	private KillNode(Image i) {
		super(i);

	}

	public static KillNode  getNode()
	{
		if (img == null) {
			try {
				img = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
						+ "\\component\\img_death.png")));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
		return new KillNode(img);
		
	}

}


