package be.vgqd.vrjam.game.chogath.contract.impl;

import be.vgqd.vrjam.game.chogath.contract.GameContract;
import be.vgqd.vrjam.game.chogath.contract.PlayerLolContract;
import be.vgqd.vrjam.game.chogath.utils.Direction;
import be.vgqd.vrjam.game.chogath.utils.Position;

public class AdapterMode implements PlayerLolContract, GameContract {

	@Override
	public void playerTeleport(double x, double y) {
	}

	@Override
	public void playerMove(double x, double y) {
	}

	@Override
	public void playerMove(Direction toward) {

	}

	@Override
	public void playerAttack(double x, double y) {

	}

	@Override
	public void playerAttack(Direction toward) {

	}

	@Override
	public void playerAttackHeroes(double x, double y) {

	}

	@Override
	public void sendMessage(String msg) {

	}

	@Override
	public void sendMessage(String msg, boolean sendAll) {

	}

	@Override
	public void sendSignal(Signal value) {

	}

	@Override
	public void switchToHome() {


	}

	@Override
	public void switchToInGame() {

		
	}

	

	@Override
	public void displayScore() {

	}

	@Override
	public void resetCalibration() {

	}

	@Override
	public void activatePrimaryPower1() {

	}

	@Override
	public void activatePrimaryPower2() {

	}

	@Override
	public void activatePrimaryPower3() {

	}

	@Override
	public void activatePrimaryPower4() {

	}

	@Override
	public void activateSecondaryPower1() {

	}
	

	@Override
	public void activateSecondaryPower2() {

	}

	@Override
	public void activateObject1() {

	}

	@Override
	public void activateObject2() {

	}

	@Override
	public void activateObject3() {

	}

	@Override
	public void activateObject4() {

	}

	@Override
	public void activateObject5() {

	}

	@Override
	public void activateObject6() {

	}

	@Override
	public void displayDialMessage(String msg) {
		System.out.println(msg);
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void movePlayerCursor(Position pt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Position getPlayerCursor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void displayCredit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub
		
	}

	public  void startRecordingData()
	{
		
	}

	@Override
	public void addChampion() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMushRoom() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addMovingAttack() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cameraMove(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cameraMove(Direction toward) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deactivateSecondaryPower1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayInterface(double value) {
		// TODO Auto-generated method stub
		
	}

}
