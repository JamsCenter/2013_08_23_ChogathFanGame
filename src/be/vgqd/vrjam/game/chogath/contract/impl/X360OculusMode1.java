package be.vgqd.vrjam.game.chogath.contract.impl;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import net.java.games.input.Component.Identifier;
import net.java.games.input.Controller;
import be.vgqd.vrjam.game.chogath.Game;
import be.vgqd.vrjam.game.chogath.Game.SceneType;
import be.vgqd.vrjam.game.chogath.data.InGame;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet;
import be.vgqd.vrjam.game.chogath.data.modele.Cursor;
import be.vgqd.vrjam.game.chogath.data.modele.InGameData;
import be.vgqd.vrjam.game.chogath.data.modele.Map;
import be.vgqd.vrjam.game.chogath.data.modele.Power;
import be.vgqd.vrjam.game.chogath.data.modele.Power.Type;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.oculus.OculusControllerData;
import be.vgqd.vrjam.game.chogath.oculus.OculusTo2D;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.oculus.x360.X360ControllerData;
import be.vgqd.vrjam.game.chogath.scene.HomeScene;
import be.vgqd.vrjam.game.chogath.utils.Direction;
import be.vgqd.vrjam.game.chogath.utils.PoleDirection;
import de.fruitfly.ovr.OculusRift;

/**
 * Cette classe vas interpr�t�er les mouvements de l'Oculus, du Clavier et du
 * Jinput pour le traduire par des commandes � la classe h�ritant de GAME
 */
public class X360OculusMode1 extends AdapterMode {

	private OculusRift oculusC;
	// private OculusControllerData oculusData;
	private Controller x360C;
	private X360ControllerData x360Data;

	private boolean allowRecord = true;
	private boolean x360Connected;

	public X360OculusMode1() {

		super();
		oculusC = OculusControllerData.getOculusInstance();
		// oculusData = OculusControllerData.getInstance();
		x360C = X360ControllerData.getX360Instance();
		if (x360C != null)
			x360Connected = true;
		x360Data = X360ControllerData.getInstance();

		if (x360C == null || !X360ControllerData.isConnected()) {
			allowRecord = false;
			// throw new IllegalArgumentException(
			// "Important devise not connected: X360 Controller");
		}
		if (oculusC == null || !OculusControllerData.isConnected()) {
			allowRecord = false;
			// throw new IllegalArgumentException(
			// "Important devise not connected: Oculus Rift");
		}
	}

	public final AnimationTimer refreshTimer = new AnimationTimer() {

		private int frameTime = 40;

		@Override
		public void handle(long arg0) {
			if (allowRecord) {
				if (oculusC != null && OculusControllerData.isConnected()) {
					oculusC.poll();
					OculusControllerData.saveLastPositionRYP(oculusC.getRoll(),
							oculusC.getYaw(), oculusC.getPitch());

				}

				if (x360C == null || !x360Connected) {
					x360C = X360ControllerData.getX360Instance(true);
				}

				else {
					x360Connected = x360C.poll();

					x360Data.setArrows(x360C.getComponent(Identifier.Axis.POV)
							.getPollData());
					x360Data.getLeft()
							.setX(x360C.getComponent(Identifier.Axis.X)
									.getPollData());
					x360Data.getLeft()
							.setY(x360C.getComponent(Identifier.Axis.Y)
									.getPollData());
					x360Data.getRight().setX(
							x360C.getComponent(Identifier.Axis.RX)
									.getPollData());
					x360Data.getRight().setY(
							x360C.getComponent(Identifier.Axis.RY)
									.getPollData());

					x360Data.setA(x360C.getComponent(Identifier.Button._0)
							.getPollData() != 0.0);
					x360Data.setB(x360C.getComponent(Identifier.Button._1)
							.getPollData() != 0.0);
					x360Data.setX(x360C.getComponent(Identifier.Button._2)
							.getPollData() != 0.0);
					x360Data.setY(x360C.getComponent(Identifier.Button._3)
							.getPollData() != 0.0);
					x360Data.setLb(x360C.getComponent(Identifier.Button._4)
							.getPollData() != 0.0);
					x360Data.setRb(x360C.getComponent(Identifier.Button._5)
							.getPollData() != 0.0);
					x360Data.setMenu(x360C.getComponent(Identifier.Button._6)
							.getPollData() != 0.0);
					x360Data.setStart(x360C.getComponent(Identifier.Button._7)
							.getPollData() != 0.0);
					x360Data.setLeftPull(x360C.getComponent(
							Identifier.Button._8).getPollData() != 0.0);
					x360Data.setRightPull(x360C.getComponent(
							Identifier.Button._9).getPollData() != 0.0);

					final double z = x360C.getComponent(Identifier.Axis.Z)
							.getPollData();
					if (z > 0) {
						x360Data.setLt(z);
						x360Data.setRt(0.0);
					} else if (z < 0.0) {
						x360Data.setLt(0.0);
						x360Data.setRt(Math.abs(z));
					}

				}
				translateDataIntoPlayerAction();
				try {
					Thread.sleep(frameTime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	};

	@Override
	public void start() {

		Game.getInstance().start();

	}

	private void translateDataIntoPlayerAction() {
		interpretOculusMovement();

		interpretX360Buttons();

	}

	private void interpretX360Buttons() {
		if (Game.getInstance().getSceneType() == SceneType.HOME) {
			if (x360Data.isStart()) {
				start();
			}
			if (x360Data.isY()) {
				start();
			}
			if (x360Data.isX() || x360Data.isB()) {
				displayCredit();
			}
			if (x360Data.isMenu()) {
				displayCredit();
			}
			if (x360Data.isA()) {
				exit();
			}

		}

		if (Game.getInstance().getSceneType() == SceneType.INGAME) {
			if (!x360Data.isLeftPull()) {
				if (x360Data.isLb()) {
					activateSecondaryPower1();
				}
				if (x360Data.isRb()) {
					activateSecondaryPower2();
				}
				if (x360Data.isA()) {
					activatePrimaryPower1();
				}
				if (x360Data.isX()) {
					activatePrimaryPower2();
				}
				if (x360Data.isY()) {
					activatePrimaryPower3();
				}
				if (x360Data.isB()) {
					activatePrimaryPower4();
				}
			} else {
				if (x360Data.isLb()) {
					activateObject1();
				}
				if (x360Data.isRb()) {
					activateObject2();
				}
				if (x360Data.isA()) {
					activateObject3();
				}
				if (x360Data.isX()) {
					activateObject4();
				}
				if (x360Data.isY()) {
					activateObject5();
				}
				if (x360Data.isB()) {
					activateObject6();
				}

			}
			if (x360Data.isRightPull()) {
				InGame.getInstance().addOneLevelOfDifficulty();
			}

			if (x360Data.isMenu()) {
				switchToHome();
			}
			if (x360Data.isStart()) {
				Game.getInstance().pause();
			}
			// if (x360Data.getLt() > 0.9) {
			// // playerAttack(Cursor.getOculusCursorInstance().getPosition());
			//
			// } else
			if (x360Data.getLt() > 0.1) {

				Cursor c = Cursor.getOculusCursorInstance();
				playerMove(c.getX(), c.getY());

			}
			if (x360Data.getRt() >= 0.0) {

				displayInterface(Math.abs(x360Data.getRt()));

			}
			final Direction dl = x360Data.getLeft();
			final Direction dr = x360Data.getRight();

			if (Math.abs(dl.getX()) > 0.1 || Math.abs(dl.getY()) > 0.1) {

				playerMove(dl);
			}

			if (Math.abs(dr.getX()) > 0.10 || Math.abs(dr.getY()) > 0.10) {
				// PoleDirection pole = Direction.getPoleDirection(dr.getX(),
				// -dr.getY());
				// // if (pole == PoleDirection.SE)
				// // // sendMessage("ss");
				// else if (pole == PoleDirection.NO)
				// // sendMessage("Help here please !");
				// else if (pole == PoleDirection.SO)
				// // sendSignal(null);
			}
			playerMove(x360Data.getArrows());
		}
		Direction d = x360Data.getArrows();
		if (d.getPoleDirection() == PoleDirection.N) {
			resetCalibration();
		}

	}

	private void interpretOculusMovement() {

		Direction d = OculusTo2D.getDirection(
				OculusControllerData.getFrontview(),
				OculusControllerData.getLastPositionRYP());

		cameraMove(d);

	}

	@Override
	public void playerTeleport(double x, double y) {

		InGame.getInstance().teleportPlayer(x, y);
	}

	@Override
	public void playerMove(double x, double y) {

		Cursor c = Cursor.getOculusCursorInstance();
		InGame.getInstance().movePlayer(c.getX(), c.getY(),
				Math.abs(x360Data.getLt()));
	}

	@Override
	public void playerMove(Direction toward) {
		Direction d = new Direction(toward.getX(), -toward.getY());
		d.setIntensity(1.0);
		InGame.getInstance().movePlayer(d);

	}

	@Override
	public void cameraMove(Direction toward) {

		// InGame.getInstance().moveCamera(toward);
		StereoNode<Node> map = GUI.get(InGameData.getInstance());
		final Map m = InGameData.getInstance().getMap();
		if (map != null && m != null) {
			double witdh = m.getWidth(), height = m.getHeight();
			double x = -witdh / 2 - toward.getX() * witdh;
			double y = -height / 2 + toward.getY() * height * 1.05;

			map.setLayout(x, y);

			Cursor.getOculusCursorInstance().setX(-x);
			Cursor.getOculusCursorInstance().setY(-y);
		}
	}

	@Override
	public void playerAttack(double x, double y) {
		playerMove(x, y);
	}

	@Override
	public void playerAttack(Direction toward) {
		playerMove(toward);
	}

	@Override
	public void playerAttackHeroes(double x, double y) {

	}

	@Override
	public void sendMessage(String msg) {
		System.out.println("Message: " + msg);

	}

	@Override
	public void sendMessage(String msg, boolean sendAll) {
		System.out.println("Message (All): " + msg);

	}

	@Override
	public void sendSignal(Signal value) {
		System.out.println("Ping !!");
		// Sounds.play("Ping");

	}

	@Override
	public void switchToHome() {
		Game.getInstance().switchScene(SceneType.HOME);

	}

	@Override
	public void switchToInGame() {
		Game.getInstance().switchScene(SceneType.INGAME);

	}

	@Override
	public void displayInterface(double value) {

		InGame.getInstance().displayInterface(value);
		InGame.getInstance().refreshPowers(Type.CAPACITY1, value);

	}

	@Override
	public void displayScore() {
		System.out.println("Show score");

	}

	@Override
	public void resetCalibration() {
		OculusControllerData.saveCurrentRYPAsFrontView();

	}

	@Override
	public void activatePrimaryPower1() {

		InGame.getInstance().activatePower(Power.Type.POWER1);
	}

	@Override
	public void activatePrimaryPower2() {

		InGame.getInstance().activatePower(Power.Type.POWER2);

	}

	@Override
	public void activatePrimaryPower3() {

		InGame.getInstance().activatePower(Power.Type.POWER3);
	}

	@Override
	public void activatePrimaryPower4() {

		InGame.getInstance().activatePower(Power.Type.POWER4);
	}

	@Override
	public void activateSecondaryPower1() {

		InGame.getInstance().activatePower(Power.Type.CAPACITY1);

	}

	@Override
	public void activateSecondaryPower2() {

		InGame.getInstance().activatePower(Power.Type.CAPACITY2);
	}

	@Override
	public void activateObject1() {

	}

	@Override
	public void activateObject2() {

	}

	@Override
	public void activateObject3() {

	}

	@Override
	public void activateObject4() {

	}

	@Override
	public void activateObject5() {

	}

	@Override
	public void activateObject6() {

	}

	@Override
	public void addChampion() {
		InGame.getInstance().addChampion();
	}

	@Override
	public void addMushRoom() {

		InGame.getInstance().addBullet(Bullet.Type.MUSHROOM);
	}

	public void addMovingAttack() {
		InGame.getInstance().addMovingAttack();
	}

	@Override
	public void displayCredit() {
		HomeScene sc = (HomeScene) GUI.get(SceneType.HOME);
		sc.displayCredit();
	}

	@Override
	public void pause() {

		Game.getInstance().pause();

	}

	@Override
	public void exit() {
		Game.getInstance().exit();

	}

	public void startRecordingData() {
		refreshTimer.start();

	}
}
