package be.vgqd.vrjam.game.chogath.contract.impl;


public abstract class Modes {

	private static X360OculusMode1 mode1=null;
	public static X360OculusMode1 getContactMode1(){
		if(mode1==null)mode1=new X360OculusMode1();
		return mode1;
	}
	
	
	
	public abstract boolean connectGameToDevice();
	public abstract void startRecordingData();
}
