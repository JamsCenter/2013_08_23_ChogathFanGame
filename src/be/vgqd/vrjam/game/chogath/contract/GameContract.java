package be.vgqd.vrjam.game.chogath.contract;

import be.vgqd.vrjam.game.chogath.utils.Position;

public interface GameContract {


	public void switchToHome();
	public void switchToInGame();
	public void displayInterface(double value);
	public void displayScore();
	public void displayCredit();
	public void displayDialMessage(String msg);
	public void resetCalibration();
	public void start();
	public void pause();
	public void exit();

	public void movePlayerCursor(Position pt);
	public Position getPlayerCursor();
	
	public void  addChampion();
	public void  addMushRoom() ;
	public void  addMovingAttack() ;
}
