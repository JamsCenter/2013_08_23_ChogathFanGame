package be.vgqd.vrjam.game.chogath.contract;

public interface PlayerLolContract extends PlayerContract{

	public void activatePrimaryPower1();
	public void activatePrimaryPower2();
	public void activatePrimaryPower3();
	public void activatePrimaryPower4();
	public void activateSecondaryPower1();
	public void activateSecondaryPower2();

	public void activateObject1();
	public void activateObject2();
	public void activateObject3();
	public void activateObject4();
	public void activateObject5();
	public void activateObject6();
	

	
	
	
}
