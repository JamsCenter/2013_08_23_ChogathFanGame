package be.vgqd.vrjam.game.chogath.contract;

import be.vgqd.vrjam.game.chogath.utils.Direction;

public interface PlayerContract {
	public enum Signal {
		ALERT, WITHDRAWAL
	};

	public void playerTeleport(double x, double y );

	public void playerMove(double x, double y);

	public void playerMove(Direction toward);

	public void cameraMove(double x, double y);

	public void cameraMove(Direction toward);

	public void playerAttack(double x, double y);

	public void playerAttack(Direction toward);

	public void playerAttackHeroes(double x, double y);

	public void sendMessage(String msg);

	public void sendMessage(String msg, boolean sendAll);

	public void sendSignal(Signal value);

	void deactivateSecondaryPower1();

}
