package be.vgqd.vrjam.game.chogath.utils;

import java.util.Random;

import javafx.geometry.Point2D;

import be.vgqd.vrjam.game.chogath.data.modele.InGameData;

/**
 * @author Eloi Warning, most of the methode work fine only if the x, y is X ->
 *         and y � axe In a future version, make the Direction working with all
 *         axe direction.
 */
public class Direction {
	// public enum AxeDirectionX{ToLeft,ToRight}
	// public enum AxeDirectionY{ToTop,ToBot}
	// private AxeDirectionX axeX = AxeDirectionX.ToRight;
	private double x;
	// private AxeDirectionY axey = AxeDirectionY.ToTop;
	private double y;
	private double intensity=1.0;

	public Direction() {
		super();
	}

	public Direction(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void setX(double x) {

		this.x = checkBounds(x);
	}

	public void setY(double y) {
		this.y = checkBounds(y);
	}

	private double checkBounds(double value) {

		if (value > 1.0)
			return 1.0;
		else if (value < -1.0)
			return -1.0;
		return value;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getIntensity() {
		return intensity;
	}

	public void setIntensity(double intensity) {
		if (intensity < 0)
			this.intensity = 0;
		else
			this.intensity = intensity;
	}

	public static void setDirection(Direction d, double startX, double startY,
			double endX, double endY) {
		double xprime = endX - startX;
		double yprime = endY - startY;
		Direction.setDirection(d, xprime, yprime);

	}

	public static void setDirection(Direction d, double x, double y) {
		setDirectionOnAngle(d, getAngle(x, y));
	}

	public double getAngle() {
		return Direction.getAngle(this);
	}

	public static double getAngle(final Direction d) {

		double x = d.getX();
		double y = d.getY();

		return getAngle(x, y);
	}

	public static double getAngle(double x, double y) {

		if (x == 0 || y == 0) {
			if (x == 0 && y > 0)
				return 90;
			if (x == 0 && y < 0)
				return 270;
			if (y == 0 && x > 0)
				return 0;
			if (y == 0 && x < 0)
				return 180;

		} else if (x > 0 && y > 0) {
			return Math.toDegrees(Math.atan(y / x));

		} else if (x < 0 && y > 0) {

			return -Math.toDegrees(Math.atan(x / y)) + 90;

		} else if (x < 0 && y < 0) {

			return Math.toDegrees(Math.atan(y / x)) + 180;

		} else if (x > 0 && y < 0) {

			return -Math.toDegrees(Math.atan(x / y)) + 270;

		}

		return 0;

	}

	public void setDirectionOnAngle(final double angle) {
		setDirectionOnAngle(this, angle);
	}

	public static void setDirectionOnAngle(final Direction d, final double angle) {
		double rad = 0;
		if (angle <= 90 && angle >= 0) {
			rad = Math.toRadians(angle);
			d.setX(Math.cos(rad));
			d.setY(Math.sin(rad));

		} else if (angle <= 180) {
			rad = Math.toRadians(angle - 90);
			d.setY(Math.cos(rad));
			d.setX(-Math.sin(rad));

		} else if (angle <= 270) {
			rad = Math.toRadians(angle - 180);
			d.setX(-Math.cos(rad));
			d.setY(-Math.sin(rad));

		} else if (angle <= 360) {
			rad = Math.toRadians(angle - 270);
			d.setY(-Math.cos(rad));
			d.setX(Math.sin(rad));

		}

	}

	public double getradius() {
		return Math.sqrt(x * x + y * y);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Direction [x=");
		builder.append(String.format("%.2f", x));
		builder.append(", y=");
		builder.append(String.format("%.2f", y));
		builder.append(", intensity=");
		builder.append(intensity);
		builder.append("]");
		return builder.toString();
	}

	public PoleDirection getPoleDirection() {
		return Direction.getPoleDirection(getX(), getY());
	}

	public static PoleDirection getPoleDirection(double x, double y) {

		double angle = getAngle(x, y);
		if (angle >= 0 && angle <= 22.5)
			return PoleDirection.E;
		else if (angle <= 67.5)
			return PoleDirection.NE;
		else if (angle <= 112.5)
			return PoleDirection.N;
		else if (angle <= 157.5)
			return PoleDirection.NO;
		else if (angle <= 202.5)
			return PoleDirection.O;
		else if (angle <= 247.5)
			return PoleDirection.SO;
		else if (angle <= 292.5)
			return PoleDirection.S;
		else if (angle <= 337.5)
			return PoleDirection.SE;
		else
			return PoleDirection.E;

	}

	public void inverse() {
		inverseX();
		inverseY();

	}

	public void inverseY() {
		y *= -1;

	}

	public void inverseX() {
		x *= -1;

	}

	public void joinData(Direction... directions) {

		if (directions.length > 0) {
			double x = this.x, y = this.y, intensity = this.intensity;
			for (Direction direction : directions) {
				x += direction.x;
				y += direction.y;
				intensity += direction.intensity;
			}
			this.x = x / (double)(directions.length+1);
			this.y = y / (double)(directions.length+1);

			this.intensity = intensity /((double) directions.length+1);
		}

	}

	public double getPseudoIntensityLevel()
	{
		return Math.sqrt(x*x+y*y);
	}

	public void setWith(Direction playerDirection) {
		
		x=playerDirection.x;
		y=playerDirection.y;
		intensity= playerDirection.intensity;
		
	}

	public static void setDirectionOnRandom(Direction direction,Random rand) {
		
		double x =rand.nextDouble();
		boolean xsigne= rand.nextBoolean();
		double y =InGameData.getRandomInstance().nextDouble();
		boolean ysigne= rand.nextBoolean();
		direction.setX(x*(xsigne?-1:1));
		direction.setY(y*(ysigne?-1:1));
		
	}

	public static void setDirection(Direction tosetup, Direction from) {
		tosetup.setWith(from);
		
	}

	public static Point2D getDestination(Point2D pt, Direction direction,int rayon) {
		
		return new Point2D(pt.getX()+direction.getX()*rayon,pt.getY()+direction.getY()*rayon );
	}
	
}
